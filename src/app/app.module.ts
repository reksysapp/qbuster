import { FileTransfer } from '@ionic-native/file-transfer';
import { EmojiPickerComponentModule } from './../components/emoji-picker/emoji-picker.module';
// import { RelativeTime } from './../pipes/relative-time';
import { SplashPage } from './../pages/splash/splash';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { PincodeSearchPage } from './../pages/pincode-search/pincode-search';
import { ProviderLoginPage } from './../pages/provider-login/provider-login';
import { ProviderSpecialistsPage } from './../pages/provider-specialists/provider-specialists';
import { CustomerAddMemberPage } from './../pages/customer-add-member/customer-add-member';
import { CustomerMemberPage } from './../pages/customer-member/customer-member';
import { AppointmentExpiredPage } from './../pages/appointment-expired/appointment-expired';
import { AppointmentActivePage } from './../pages/appointment-active/appointment-active';
import { ViewTokenPage } from './../pages/view-token/view-token';
import { ServiceConformationPage } from './../pages/service-conformation/service-conformation';
import { ProviderMenuPage } from './../pages/provider-menu/provider-menu';
import { CustomerMenuPage } from './../pages/customer-menu/customer-menu';
import { ProviderHomePage } from './../pages/provider-home/provider-home';
import { SectorSelectionPage } from './../pages/sector-selection/sector-selection';
import { HourlyAppoinmentPage } from './../pages/hourly-appoinment/hourly-appoinment';
import { ChatPage } from './../pages/chat/chat';
import { ClientSessionPage } from './../pages/client-session/client-session';
import { DetailsPage } from './../pages/details/details';
import { ProfilePage } from './../pages/profile/profile';
import { VerificationPage } from './../pages/verification/verification';
import { ResetPasswordPage } from './../pages/reset-password/reset-password';
import { ForgotPasswordPage } from './../pages/forgot-password/forgot-password';
import { ContactUsPage } from './../pages/contact-us/contact-us';
import { TokenPage } from './../pages/token/token';
import { MyBookingPage } from './../pages/my-booking/my-booking';
import { AppointmentPage } from './../pages/appointment/appointment';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ClientServiceProvider } from '../providers/client/client-service';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginPage } from '../pages/login/login';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CustomerServiceProvider } from '../providers/customer-service/customer-service';
import { ToastServiceProvider } from '../providers/toast-service/toast-service';
import { JsonServicesProvider } from '../providers/json-services/json-services';
import { DatePickerModule, DatePickerDirective } from 'ion-datepicker';
import { TypeaheadModule } from 'ngx-bootstrap';
import { SuperTabsModule, SuperTabsController } from 'ionic2-super-tabs';
import { AppointmentServiceProvider } from '../providers/appointment-service/appointment-service';
import { ServeTokenPage } from '../pages/serve-token/serve-token';
import { TokenSessionProvider } from '../providers/token-session/token-session';
import { IonicStorageModule } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { CallNumber } from '@ionic-native/call-number';
import { ProviderAddSpecialistPage } from '../pages/provider-add-specialist/provider-add-specialist';
import { ListCustomerAppointmentsPage } from '../pages/list-customer-appointments/list-customer-appointments';
import { CustomerLoginPage } from '../pages/customer-login/customer-login';
import { BookAppointmentPage } from '../pages/book-appointment/book-appointment';
import { WheelSelector } from '@ionic-native/wheel-selector';
import { HelpPage } from '../pages/help/help';
import { FaqPage } from '../pages/faq/faq'

//chat Imports
import { ChatUserServiceProvider } from '../providers/chat-user-service/chat-user-service';
// import { SocketIoModule, SocketIoConfig } from 'ng-socket-io'; //socket import
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

// const config: SocketIoConfig = { url: 'http://192.168.1.2:4001', options: {} }; //socket configuration
 const config: SocketIoConfig = { url: 'http://exactcloud.com:4001', options: {} }; //socket configuration
import { EmojiProvider } from '../providers/emoji';
import { PrivateMessagePage } from '../pages/private-message/private-message';
import { ProviderChatPage } from '../pages/provider-chat/provider-chat';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Base64 } from '@ionic-native/base64';
import { Crop } from '@ionic-native/crop';

import { Geolocation } from '@ionic-native/geolocation';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { AddLocationPage } from '../pages/add-location/add-location';
import { InternetInterceptorProvider } from '../providers/internet-interceptor/internet-interceptor';
import { ProviderPrivateMessagePage } from '../pages/provider-private-message/provider-private-message';
import { DirectivesModule } from '../directives/directives.module';
import { TermsAndConditionPage } from '../pages/terms-and-condition/terms-and-condition';
import { MemberDetailsPage } from '../pages/member-details/member-details';
import { VideocallPage } from '../pages/videocall/videocall';
import { WebrtcService } from '../providers/WebrtcService';
import { Device } from '@ionic-native/device';
import { AndroidPermissions } from '@ionic-native/android-permissions';


// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    AppointmentPage,
    MyBookingPage,
    TokenPage,
    ContactUsPage,
    ForgotPasswordPage,
    ResetPasswordPage,
    VerificationPage,
    ProfilePage,
    DetailsPage,
    ClientSessionPage,
    ChatPage,
    HourlyAppoinmentPage,
    SectorSelectionPage,
    ProviderHomePage,
    ServeTokenPage,
    CustomerMenuPage,
    ProviderMenuPage,
    ServiceConformationPage,
    ViewTokenPage,
    VideocallPage,
    AppointmentActivePage,
    AppointmentExpiredPage,
    CustomerMemberPage,
    CustomerAddMemberPage,
    ProviderSpecialistsPage,
    ProviderAddSpecialistPage,
    ListCustomerAppointmentsPage,
    CustomerLoginPage,
    ProviderLoginPage,
    BookAppointmentPage,
    PincodeSearchPage,
    SplashPage,
    HelpPage,
    FaqPage,
    AddLocationPage,
    ProviderChatPage,
    PrivateMessagePage,
    ProviderPrivateMessagePage,
    TermsAndConditionPage,
    MemberDetailsPage
    // RelativeTime
  ],
  imports: [
   
    BrowserModule,
    DatePickerModule,
    EmojiPickerComponentModule,
    
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    HttpClientModule,
    TypeaheadModule.forRoot(),
    SuperTabsModule.forRoot(),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    DirectivesModule,

    //chat imports
    SocketIoModule.forRoot(config), //socket
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    VideocallPage,
    TabsPage,
    LoginPage,
    AppointmentPage,
    MyBookingPage,
    TokenPage,
    ContactUsPage,
    ForgotPasswordPage,
    ResetPasswordPage,
    VerificationPage,
    ProfilePage,
    DetailsPage,
    ClientSessionPage,
    ChatPage,
    HourlyAppoinmentPage,
    SectorSelectionPage,
    ProviderHomePage,
    ServeTokenPage,
    CustomerMenuPage,
    ProviderMenuPage,
    ServiceConformationPage,
    ViewTokenPage,
    AppointmentActivePage,
    AppointmentExpiredPage,
    CustomerMemberPage,
    CustomerAddMemberPage,
    ProviderSpecialistsPage,
    ProviderAddSpecialistPage,
    ListCustomerAppointmentsPage,
    CustomerLoginPage,
    ProviderLoginPage,
    BookAppointmentPage,
    PincodeSearchPage,
    SplashPage,
    HelpPage,
    FaqPage,
    AddLocationPage,
    ProviderChatPage,
    PrivateMessagePage,
    ProviderPrivateMessagePage,
    TermsAndConditionPage,MemberDetailsPage
  ],
  providers: [
    
    AndroidPermissions,
    LocalNotifications,
    Device,
    WebrtcService,
    StatusBar,
    InAppBrowser,
    Geolocation,
    SplashScreen, HttpClient,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ClientServiceProvider,
    CustomerServiceProvider,
    ToastServiceProvider,
    JsonServicesProvider,
    DatePickerDirective,
    SuperTabsController,
    AppointmentServiceProvider,
    ServeTokenPage,
    TokenSessionProvider,
    Network,
    CallNumber,
    WheelSelector,
    Camera,
    //chat providers
    ChatUserServiceProvider,
    EmojiProvider,
    InAppBrowser,
    LaunchNavigator,
    FileTransfer,
    Base64,
    Crop,
    
    InternetInterceptorProvider,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InternetInterceptorProvider,
      multi: true
    }
  ]
})
export class AppModule { }
