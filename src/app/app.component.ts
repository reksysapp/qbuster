import { LoginPage } from './../pages/login/login';
import { ToastServiceProvider } from './../providers/toast-service/toast-service';
import { Component, ViewChild } from '@angular/core';
import { Platform, Config, Nav, ModalController, AlertController, App, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { Network } from '@ionic-native/network';
import { ServeTokenPage } from '../pages/serve-token/serve-token';
import { AboutPage } from '../pages/about/about';
import { MemberDetailsPage } from '../pages/member-details/member-details';
import { Socket } from 'ngx-socket-io';
import { Device } from '@ionic-native/device';
import { AppointmentServiceProvider } from '../providers/appointment-service/appointment-service';
import { LocalNotifications } from '@ionic-native/local-notifications';
import moment from 'moment';
import { IonicPage, NavParams, NavController } from 'ionic-angular';
import { VideocallPage } from '../pages/videocall/videocall';
import { ChatUserServiceProvider } from '../providers/chat-user-service/chat-user-service';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  allScheduledList = [];
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  // rootPage: any = MemberDetailsPage;
  public counter = 0;

  constructor(public platform: Platform,public chatService:ChatUserServiceProvider,
    public appService: AppointmentServiceProvider, 
    private device: Device, private config: Config, public network: Network, public toast: ToastServiceProvider,
    private translate: TranslateService, statusBar: StatusBar, private socket: Socket, splashScreen: SplashScreen, modalCtrl: ModalController,
    public alertCtrl: AlertController, public app: App, public toastCtrl: ToastController, private localNotifications: LocalNotifications) {
    setInterval(() => {
     

      this.appService.getCallReminder(this.device.uuid).subscribe(res => {
        if (res[0].status&& res[0].callerName) {
          // this.toast.presentToast(res[0].callingId +"is calling!!")
          // this.toast.showAlert(res[0].callingId + "\tis calling!!");

          let alert = this.alertCtrl.create({
            title: 'Notification',
            message:res[0].callerName + "\tis calling!!",
            buttons: [
              {
                text: 'Decline',
                role: 'cancel',
                handler: data => {
                  this.nav.pop()
                }
              },
              {
                text: 'Answer',
                handler: data => {
                 this.nav.push(VideocallPage,{"to":{_id:res[0].callingId},"from": {_id:res[0].calledId}});
                }
              }

            ]
          });
          alert.present();



        }
       
      })
      
     
    }, 10000)
    setInterval(() => {
      // this.device.uuid
      this.appService.getReminderList(this.device.uuid).subscribe(res => {
        console.log("reminderList", res);
        // this.toast.presentToast(this.device.uuid);

        res.forEach((element, index) => {
          let convertedTime = moment(element.aptSlot, 'hh:mm A').format('HH:mm')
          console.log(convertedTime);
          let sliceDate = new Date(element.aptDate).toLocaleDateString()
          let momentDate = moment(sliceDate + " " + convertedTime, 'DD/MM/YYYY HH:mm').subtract(10, 'minutes').format()
          let scheduledList = {
            id: index + 20000 + 1,
            text: 'provider have an Appointment',
            trigger: { at: new Date(momentDate) },
            led: 'FF0000',
            sound: null
          }
          this.allScheduledList.push(scheduledList)
        });
        this.localNotifications.requestPermission().then((permission) => {
          // this.localNotifications.clearAll().then(() => {


          // })
          if (this.allScheduledList.length > 0)
            this.localNotifications.schedule(this.allScheduledList)

        })
      })
      // this.device.uuid
      // this.socket.emit('deviceId',this.device.uuid)

    }, 10000);

    platform.ready().then(() => {
      let connectSubscription = this.network.onConnect().subscribe(() => {
        this.toast.presentToast("Connected to Internet")
        setTimeout(() => {
        }, 3000);
      });

      let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        this.toast.showError("No Internet Connection")
      });
      statusBar.styleDefault();
      splashScreen.hide();
    });
    //Rincy to handle device's back buttom
    // platform.registerBackButtonAction(() => {

    //   let nav = app.getActiveNavs()[0];
    //   let activeView = nav.getActive();
    //   console.log("active view in back button    :", activeView.name)
    //   if (activeView.name === "LoginPage") {
    //     console.log("inside if in back button")
    //     // if (nav.canGoBack()){ //Can we go back?
    //     //     nav.pop();
    //     // } else {
    //     const alert = this.alertCtrl.create({
    //       title: 'App termination',
    //       message: 'Do you want to close the app?',
    //       buttons: [{
    //         text: 'Cancel',
    //         role: 'cancel',
    //         handler: () => {
    //           console.log('Application exit prevented!');
    //         }
    //       }, {
    //         text: 'Close App',
    //         handler: () => {
    //           this.platform.exitApp(); // Close this application
    //         }
    //       }]
    //     });
    //     alert.present();
    //     // }
    //   }
    // });
    this.initTranslate();
    platform.registerBackButtonAction(() => {
      if (this.counter == 0) {
        this.counter++;
        this.presentToast();
        setTimeout(() => { this.counter = 0 }, 3000)
      } else {
        console.log("exitapp");
        platform.exitApp();
      }
    }, 0)
    // });

  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to exit",
      duration: 3000,
      position: "middle"
    });
    toast.present();
  }
  // }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    }
    else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }
}
