import { ClientModel } from '../../model/client-model';
import { map } from 'rxjs/operators/map';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class JsonServicesProvider {

  client: ClientModel;
  public options: any;
  public token: String = "";


  constructor(public http: HttpClient) {

    this.client = ClientModel.createBlank();
  }

  getIndustryList(): Observable<ClientModel[]> {
    const msgListUrl = 'assets/mock/mockIndustry.json';
    return this.http.get<any>(msgListUrl)
      .pipe(map(response => response.array));
  }

  getStatesList(): Observable<ClientModel[]> {
    const msgListUrl = 'assets/mock/mockStates.json';
    return this.http.get<any>(msgListUrl)
      .pipe(map(response => response.array));
  }

  getCitiesList(): Observable<any[]> {
    const msgListUrl = 'assets/mock/mockCities.json';
    return this.http.get<any>(msgListUrl)
      .pipe(map(response => response));
  }
  getClientList(): Observable<any[]> {
    const msgListUrl = 'assets/data/client.json';
    return this.http.get<any>(msgListUrl)
      .pipe(map(response => response));
  }

  getFAQ(): Observable<any[]> {
    const msgListUrl = 'assets/mock/faq.json';
    return this.http.get<any>(msgListUrl)
      .pipe(map(response => response));
  }

  loadCitiesBasedZip(zipcode): Observable<any> {


    const httpOptions = {
      headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token',
      'Access-Control-Allow-Origin':'*'
     })
 };

    return this.http.get("http://postalpincode.in/api/pincode/" + zipcode, httpOptions)
      .pipe(map(response => response));
  }


}
