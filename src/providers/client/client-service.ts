import { ClientModel } from '../../model/client-model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { environment } from '../../environment/environment';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@Injectable()
export class ClientServiceProvider {

  client: ClientModel;
  clients: ClientModel[];
  private baseURL = environment.baseURL;
  public options: any;
  public token: String = "";

  constructor(public http: HttpClient, private transfer: FileTransfer) {
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'Authorization': this.token
      })
    });
    this.client = ClientModel.createBlank();
  }


  uploadImage(image, id) {
    // Destination URL
    let url = this.baseURL + 'provider/displayImage';

    var options: FileUploadOptions = {
      fileKey: 'image',
      chunkedMode: false,
      mimeType: 'multipart/form-data',
      params: { 'type': 'jpg', 'fileLocation': image, "id": id },
      httpMethod: 'POST'
    };

    const fileTransfer: FileTransferObject = this.transfer.create();
    // Use the FileTransfer to upload the image
    return fileTransfer.upload(image, url, options);
  }

  

  setClient(client) {
    console.log('inside set client', JSON.stringify(client, null, 2));
    // console.log(client);
    this.client = client;
  }

  getClient(): ClientModel {
    return this.client
  }

  getStates(client: ClientModel): Observable<ClientModel[]> {
    const requestPayload = {
      "providerSector": client.providerDetails.providerSector,
      "city": client.providerDetails.city
    }
    return this.http.post<ClientModel[]>(this.baseURL + "provider/stateName", requestPayload, this.options)
      // return this.http.get("assets/mock/state.json")
      .map(this.processClients, this)
      // .map(res => console.log("state res ",res))
      .catch(this.handleError);
  }

  getCities(client: ClientModel): Observable<ClientModel[]> {
    const requestPayload = {
      "providerSector": client.providerDetails.providerSector,
      "state": client.providerDetails.state
    }
    return this.http.post<ClientModel[]>(this.baseURL + "provider/cityName", requestPayload, this.options)
      // return this.http.get("assets/mock/city.json")
      .map(this.processClients, this)
      // .map(res => res)
      .catch(this.handleError);
  }

  getClientNames(client: ClientModel): Observable<ClientModel[]> {
    const requestPayload = {
      "providerSector": client.providerDetails.providerSector,
      "city": client.providerDetails.city
    }
    return this.http.post<ClientModel[]>(this.baseURL + "provider/getProviderNames", requestPayload, this.options)
      // return this.http.get("assets/mock/name.json")
      .map(this.processClients, this)
      // .map(res => res)
      .catch(this.handleError);
  }

  getClientSpeciality(client: ClientModel): Observable<ClientModel[]> {
    // const requestPayload = {
    //   "clientSector":  client.providerDetails.providerSector,
    //   "city": client.providerDetails.city
    // }
    // return this.http.post<ClientModel[]>(this.baseURL + "provider/getClientNames", requestPayload, this.options)
    return this.http.get("assets/mock/speciality.json")
      // .map(this.processClients, this)
      .map(res => res)
      .catch(this.handleError);
  }

  getClientSpecialist(client: ClientModel): Observable<ClientModel[]> {
    // const requestPayload = {
    //   "clientSector":  client.providerDetails.providerSector,
    //   "city": client.providerDetails.city
    // }
    // return this.http.post<ClientModel[]>(this.baseURL + "provider/getClientNames", requestPayload, this.options)
    return this.http.get("assets/mock/specialist.json")
      // .map(this.processClients, this)
      .map(res => res)
      .catch(this.handleError);
  }

  getClientDetailsById(id): Observable<ClientModel[]> {
    return this.http.get(environment.baseURL + "provider/provider/" + id, this.options)
      .map(res => res)
      .catch(this.handleError);
  }

  getSectors(): Observable<any[]> {
    return this.http.get(environment.baseURL + "provider/providerSector", this.options)
      .map(res => res)
      .catch(this.handleError);
  }

  SignupClient(client): Observable<any> {
    // console.log("client sign up provider req   /provider  :",JSON.stringify(client,null,2))
    return this.http.post<any>(this.baseURL + "provider", client, this.options)
      .map(res => res)
      .catch(this.handleError);
  }

  SigninClient(client: ClientModel): Observable<any> {
    console.log("client sign in provider req   provider/authenticate  :", JSON.stringify(client, null, 2))
    return this.http.post<ClientModel>(this.baseURL + "provider/authenticate", client, this.options)
      .map(res => res)
      .catch(this.handleError);
  }

  getSecondaryProviders(client): Observable<any[]> {
    return this.http.get(environment.baseURL + "provider/SECONDARY/" + client, this.options)
      .map(res => res)
      .catch(this.handleError);
  }

  updateClient(client: ClientModel): Observable<ClientModel> {
    console.log("client update provider  req  :")
    return this.http.put<ClientModel>(this.baseURL + "provider", client, this.options)
      .map(this.processClient, this)
      .catch(this.handleError);
  }

  updatePassword(customer): Observable<ClientModel> {
    console.log("req inside customer/pwd   :", JSON.stringify(customer, null, 2))
    return this.http.put<ClientModel>(this.baseURL + "provider/pwd", customer, this.options)
      .map(this.processClient, this)
      .catch(this.handleError);
  }

  getMember(id): Observable<any> {
    return this.http.get(environment.baseURL + "provider/members/" + id, this.options)
      .map(res => res)
      .catch(this.handleError);
  }

  editMember(member): Observable<any> {
    // console.log("customer sign up provider")
    return this.http.put<any>(this.baseURL + "provider/members", member, this.options)
      .map(res => res)
      .catch(this.handleError);
  }

  aptInfo(data): Observable<any> {
    return this.http.post<any>(this.baseURL + "appointment/aptInfo", data, this.options)
      .map(res => res)
      .catch(this.handleError);
  }

  aptCheckin(data): Observable<any> {
    console.log("checking 123")
    return this.http.put<any>(this.baseURL + "appointment/checkin", data, this.options)
      .map(res => res)
      .catch(this.handleError);
  }

  processClients(res): ClientModel[] {
    // console.log('inside process clients');
    this.clients = res;
    // console.log(this.clients);
    return this.clients;
  }

  processClient(res): ClientModel {
    // console.log('inside process client' ,res);
    this.client = res;
    console.log(this.client);
    return this.client;
  }

  handleError(error: HttpErrorResponse) {
    let errorMsg = '';
    console.log(error);
    if (error.error && error.error.status === "Error") {
      errorMsg = error.error.message;
    } else {
      errorMsg = "Try again later or contact support @ReksysS";
    }
    return new ErrorObservable(errorMsg);

  }
}
