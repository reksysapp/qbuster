import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { environment } from '../../environment/environment';
import { CustomerModel } from '../../model/customer-model';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@Injectable()
export class CustomerServiceProvider {

  public options: any;
  public token: string = "";
  private baseURL = environment.baseURL;
  public customer: CustomerModel;

  constructor(public http: HttpClient,private transfer: FileTransfer) {
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'Authorization': this.token
      })
    });
    this.customer = null;
  }

  setCustomer(customer) {
    this.customer = customer;
  }

  getCustomer(): CustomerModel {
    return this.customer;
  }

  uploadImage(image, id) {
    // Destination URL
    let url = this.baseURL + 'customer/displayImage';

    var options: FileUploadOptions = {
      fileKey: 'image',
      chunkedMode: false,
      mimeType: 'multipart/form-data',
      params: { 'type': 'jpg', 'fileLocation': image, "id": id },
      httpMethod: 'POST'
    };

    const fileTransfer: FileTransferObject = this.transfer.create();
    // Use the FileTransfer to upload the image
    return fileTransfer.upload(image, url, options);
  }

  loginCustomer(customer: CustomerModel): Observable<any> {
    console.log("req inside customer/authenticate   :", JSON.stringify(customer, null, 2))
    return this.http.post<CustomerModel>(this.baseURL + "customer/authenticate", customer, this.options)
      .map(res => res)
      .catch(this.handleError);
  }

  SignupCustomer(customer): Observable<CustomerModel> {
    console.log("req inside customer/   :", JSON.stringify(customer, null, 2))
    return this.http.post<CustomerModel>(this.baseURL + "customer/", customer, this.options)
      .map(res => res)
      .catch(this.handleError);
  }

  getCustomerDetailsById(id): Observable<CustomerModel[]> {
    return this.http.get(environment.baseURL + "customer/customer/"+id,this.options)
    .map(res => res)
    .catch(this.handleError);
  }

  getSecondaryCustomers(customer): Observable<any[]> {
    return this.http.get(environment.baseURL + "customer/SECONDARY/" + customer, this.options)
      .map(res => res)
      .catch(this.handleError);
  }
  updateCustomer(customer): Observable<CustomerModel> {
    // console.log("customer sign up client")
    return this.http.put<CustomerModel>(this.baseURL + "customer/", customer, this.options)
      .map(this.processCustomer, this)
      .catch(this.handleError);
  }
  updatePassword(customer): Observable<CustomerModel> {
    console.log("req inside customer/pwd   :", JSON.stringify(customer, null, 2))
    return this.http.put<CustomerModel>(this.baseURL + "customer/pwd", customer, this.options)
      .map(this.processCustomer, this)
      .catch(this.handleError);
  }

  editMember(member): Observable<CustomerModel> {
    // console.log("customer sign up client")
    return this.http.put<any>(this.baseURL + "customer/members", member, this.options)
      .map(this.processCustomer, this)
      .catch(this.handleError);
  }

  getSecondaryMembers(members): Observable<any> {
    console.log("req inside members   :", JSON.stringify(members, null, 2))
    return this.http.post<any>(this.baseURL + "customer/secondaryMembers", members, this.options)
      .map(res => res, this)
      .catch(this.handleError);
  }

  processCustomer(res): CustomerModel {
    // console.log('inside process user');
    this.customer = res;
    // console.log(this.customer);
    return this.customer;
  }

  handleError(error: HttpErrorResponse) {
    let errorMsg = '';
    console.log(error);
    if (error.error && error.error.status === "Error") {
      errorMsg = error.error.message;
    } else {
      errorMsg = "Try again later or contact support @ReksysS";
    }
    return new ErrorObservable(errorMsg);

  }

}
