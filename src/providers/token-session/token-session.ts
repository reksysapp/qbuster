import { TokenSessionModel } from './../../model/token-session';
import { Injectable } from '@angular/core';
import { environment } from '../../environment/environment';
import { RequestOptions , Headers } from '@angular/http';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

@Injectable()
export class TokenSessionProvider {

  public options:any;
  private baseURL = environment.baseURL;
  public token:string="";
  public tokenSession : TokenSessionModel

  constructor(public http: HttpClient) {
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'Authorization': this.token
      })
    });

    this.tokenSession = TokenSessionModel.createBlank();
  }

  setTokenSession(tokenSession) {
    this.tokenSession = tokenSession;
  }

  getTokenSession(): TokenSessionModel {
    return this.tokenSession;
  }

  startSession(data): Observable<TokenSessionModel>{
    console.log("tokenSession/session req  :",JSON.stringify(data,null,2)
  )
    return this.http.post<TokenSessionModel>(this.baseURL + "tokenSession/session", data, this.options)
      .map(this.processtokenSession, this)
      .catch(this.handleError);
  }
  sessionCurrentToken(data): Observable<any>{
    console.log("tokenSession/session req  :",JSON.stringify(data,null,2)
  )
    return this.http.post<any>(this.baseURL + "tokenSession/currentToken", data, this.options)
      .map(this.processtokenSession, this)
      .catch(this.handleError);
  }

  tokenIncrement(id): Observable<TokenSessionModel>{
    console.log("tokenSession/nav/increment req  :",JSON.stringify(id,null,2))
    return this.http.post<TokenSessionModel>(this.baseURL + "tokenSession/nav/increment", {"_id":id}, this.options)
      .map(this.processtokenSession, this)
      .catch(this.handleError);
  }
  tokenDecrement(id): Observable<TokenSessionModel>{
    return this.http.post<TokenSessionModel>(this.baseURL + "tokenSession/nav/decrement", {"_id":id}, this.options)
      .map(this.processtokenSession, this)
      .catch(this.handleError);
  }

  endSession(id): Observable<TokenSessionModel>{
    return this.http.put<TokenSessionModel>(this.baseURL + "tokenSession/"+id, this.options)
      .map(this.processtokenSession, this)
      .catch(this.handleError);
  }

  aptInfo(data): Observable<any>{
    return this.http.post<any>(this.baseURL + "appointment/aptInfo", data, this.options)
      .map(this.processInfo, this)
      .catch(this.handleError);
  }
  served(data): Observable<any>{
    return this.http.post<any>(this.baseURL + "appointment/served", data, this.options)
      .map(this.processInfo, this)
      .catch(this.handleError);
  }

  processtokenSession(res): TokenSessionModel{
    this.tokenSession = res;
    return this.tokenSession;
  }
  processInfo(res){
    return res;

  }
  
  handleError(error: HttpErrorResponse){
    let errorMsg = '';
    console.log(error);
    if(error.error && error.error.status === "Error"){
      errorMsg = error.error.message;
    } else {
      errorMsg = "Try again later or contact support @ReksysS";
    }
    return new ErrorObservable(errorMsg);

  }

}
