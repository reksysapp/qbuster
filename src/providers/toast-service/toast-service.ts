import { ToastController, LoadingController, Loading, AlertController } from 'ionic-angular';
import { Injectable, } from '@angular/core';

@Injectable()
export class ToastServiceProvider {

  loading: Loading;
  constructor(private loadingCtrl: LoadingController, private toastCtrl: ToastController, private alertCtrl: AlertController) {
    
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {;
    });
    toast.present();
  }

  showAlert(text) {
    let alert = this.alertCtrl.create({
      // title: 'QueueBuster',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  showLoading(msg: any) {
    if(!this.loading){
        this.loading = this.loadingCtrl.create({
          content: msg,
          spinner: 'bubbles',
        });
        this.loading.present();
    }
}

dismissLoading(){
  if(this.loading){
      this.loading.dismiss();
      this.loading = null;
  }
}



  showError(text) {
    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  

  redirectToLogin() {
    //this.nav.push(LoginPage);
  }
}
