import {HttpRequest,HttpHandler,HttpEvent,HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ToastServiceProvider } from '../toast-service/toast-service';

@Injectable()
export class InternetInterceptorProvider implements HttpInterceptor {
  constructor(private toast : ToastServiceProvider) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      // check to see if there's internet
      if (!window.navigator.onLine) {
          // if there is no internet, throw a HttpErrorResponse error
          // since an error is thrown, the function will terminate here
          this.toast.presentToast("No Internet Connection")
          return Observable.throw(new HttpErrorResponse({ error: 'Internet is required.' }));


      } else {
          // else return the normal request
          return next.handle(request);
      }
  }
}
