import { CustomerModel } from './../../model/customer-model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { environment } from '../../environment/environment';
import { AppointmentModel } from '../../model/appointment-model';

@Injectable()
export class AppointmentServiceProvider {

  
  private baseURL = environment.baseURL;
  public options: any;
  public token: String = "";
  appointment: AppointmentModel;
  appointments: AppointmentModel[];

  constructor(public http: HttpClient) {
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'Authorization': this.token
      })
    });
    
    this.appointment = AppointmentModel.createBlank();
  }
  
  setAppointment(appointment) {
    
    this.appointment = appointment;
    console.log('inside set appointment',JSON.stringify(this.appointment,null,2));
  }

  getAppointment(): AppointmentModel {
    console.log('inside get appointment',JSON.stringify(this.appointment,null,2));
    return this.appointment;
  }

  bookAppointment(appoinment): Observable<AppointmentModel> {
    this.appointment = AppointmentModel.createBlank();
    this.appointment = appoinment;
    // console.log("bookappointment req   :   ",JSON.stringify(this.appointment,null,2))
    return this.http.post<any>(this.baseURL + "appointment", this.appointment, this.options)
      .map(this.processAppointment, this)
      .catch(this.handleError);
  }

  // resheduleAppointment(appoinment): Observable<AppointmentModel> {
  //   // this.appointment = AppointmentModel.createBlank();
  //   this.appointment = appoinment
  //   // console.log("reshedule req   :   ",JSON.stringify(this.appointment,null,2))
  //   return this.http.post<any>(this.baseURL + "appointment/reSchedule", this.appointment, this.options)
  //     .map(this.processAppointments, this)
  //     .catch(this.handleError);
  // }
  
  appointmentToken(appoinment): Observable<AppointmentModel> {
  let id = appoinment.providerId;
    return this.http.post(this.baseURL + "appointment/apt/list",appoinment, this.options)
      .map(this.processAny, this)
      .catch(this.handleError);
  }

  deleteAppoinment(id): Observable<AppointmentModel> {
        return this.http.delete<any>(this.baseURL + "appointment/" + id, this.options)
          .map(this.processAppointment, this)
          .catch(this.handleError);
      }

      getMaxToken(customer: any): Observable<any>{
        return this.http.post<any>(this.baseURL + "appointment/maxToken", customer, this.options)
          .map(this.processAny, this)
          .catch(this.handleError);
      }

      getTotalTokenData(customer): Observable<any> {
        return this.http.post<CustomerModel>(this.baseURL + "appointment/getUpcomingAppointmentList", customer, this.options)
       .map(this.processAny, this)
       .catch(this.handleError);
     }

     getToken(url): Observable<any[]> {
      return this.http.get(environment.baseURL + url, this.options)
        .map(this.processAppointments, this)
        .catch(this.handleError);
    }

    getCallReminder(id): Observable<any[]> {
      return this.http.post(environment.baseURL + 'chat/call/'+id, this.options)
        .map(this.processAppointments, this)
        .catch(this.handleError);
    }

    getTokenData(_id): Observable<any[]> {
      return this.http.get(environment.baseURL + "appointment/custApt/"+_id, this.options)
        .map(this.processAny, this)
        .catch(this.handleError);
    }

      processAppointment(res): AppointmentModel {
        this.appointment = res;
        return this.appointment;
      }
    
      processAppointments(res): AppointmentModel[] {
        this.appointments = res;
        return this.appointments;
      }

      processAny(res):any {
        return res
      }
      getReminderList(deviceId): Observable<any> {
        return this.http.post<any>(this.baseURL + "appointment/reminder/" + deviceId, this.options)
          .map(this.processAppointment, this)
          .catch(this.handleError);
      }
      handleError(error: HttpErrorResponse) {
        let errorMsg = '';
        console.log(error);
        if (error.error && error.error.status === "Error") {
          errorMsg = error.error.message;
        } else {
          errorMsg = "Try again later or contact support @ReksysS";
        }
        return new ErrorObservable(errorMsg);
    
      }

}
