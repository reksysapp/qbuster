import { ChatUserModel } from './../../model/chatUserModel';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { environment } from '../../environment/environment';
import 'rxjs/add/operator/catch'
import 'rxjs/add/operator/map'
import { Events } from 'ionic-angular';
import { map } from 'rxjs/operators/map';
import { Socket } from 'ngx-socket-io';

export class ChatMessage {
  messageId: string;
  userId: string;
  userName: string;
  userAvatar: string;
  toUserId: string;
  time: number | string;
  message: string;
  status: string;
}

export class UserInfo {

  constructor(
    public _id: any,
    public name: string,
    public deviceId?:any,
    public status?: string,
    public avatar?: any,
    public message?: any,
    
  ) { }

  public static createBlank() {
    let blankUser = {
      _id: null,
      name: "",
     
    }
    return blankUser;
  }
}

@Injectable()
export class ChatUserServiceProvider {

  user: ChatUserModel;
  toUser: ChatUserModel;
  private baseURL = environment.baseURL;
  public options: any;
  public token: String = "";

  constructor(private socket: Socket, public http: HttpClient) {
    console.log("Inside chat user service")
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'Authorization': this.token
      })
    });
  }

  getUser() {
    return this.user;
  }

  getToUser() {
    return this.toUser;
  }

  setUser(user) {
    this.user = user
    console.log("user set !!!!!!!!!!!!!!!")
    console.log(this.user)
  }

  setToUser(userdata) {
    this.toUser = userdata;
    console.log("toUser set !!!!!!!!!!!!!!!")
    console.log(this.toUser)
  }


  getCustomerMessageList(customer): Observable<ChatUserModel[]> {
    const requestPayload = {
      "customerID": customer._id
    }
    return this.http.post<ChatUserModel[]>(this.baseURL + "chat/user/listing", requestPayload, this.options)
      .map(this.processToUser, this)
      .catch(this.handleError);
  }

  getProviderMessageList(provider): Observable<ChatUserModel[]> {
    const requestPayload = {
      "providerID": provider._id
    }
    return this.http.post<ChatUserModel[]>(this.baseURL + "chat/provider/listing", requestPayload, this.options)
      .map(this.processToUser, this)
      .catch(this.handleError);
  }
  updateProviderCustomer(from,fromName,to,status): Observable<any> {
    const requestPayLoad={
      callFrom: from,
      callTo: to,
      status:status,
      callerName:fromName
    }
    return this.http.put<any>(this.baseURL + "chat/",requestPayLoad,this.options)
      .map(this.processToUser, this)
      .catch(this.handleError);
  }
 
  processToUser(res): ChatUserModel {
    console.log('inside process user list');
    this.toUser = res;
    console.log(this.toUser);
    return this.toUser;
  }

  getUserInfo(user): Promise<UserInfo> {
    const userInfo: UserInfo = {
      _id: user._id,
      name: user.name,
      deviceId:user.deviceId
    };
    return new Promise(resolve => resolve(userInfo));
  }

  getUserData(id,name,deviceId): Promise<UserInfo> {
    const userInfo: UserInfo = {
      _id: id,
      name: name,
      deviceId:deviceId
    };
    return new Promise(resolve => resolve(userInfo));
  }

  getToUserInfo(touser): Promise<UserInfo> {
    const toUserInfo: UserInfo = {
      _id: touser._id,
      name: touser.name
    };
    return new Promise(resolve => resolve(toUserInfo));
  }

  sendMsg(msg: ChatMessage) {
    console.log("chat service sendmsg(msg)")
    msg.status = "success";
    console.log("chat user service ---------------- starts");
    this.socket.emit('sendmsg', msg)  //storing data in database
    console.log("chat user service ---------------- ends");
    return new Promise(resolve => setTimeout(() => resolve(msg), Math.random() * 1000))
      .then(() => {
        console.log("Data stored in db");
      });
  }

  handleError(error: HttpErrorResponse) {
    let errorMsg = '';
    console.log(error);
    if (error.error && error.error.status === "Error") {
      errorMsg = error.error.message;
    } else {
      errorMsg = "API Server error. Please try again later.";
    }
    return new ErrorObservable(errorMsg);

  }
}
