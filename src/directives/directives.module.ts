import { NgModule } from '@angular/core';
import { RelativeTime } from '../pipes/relative-time';
@NgModule({
	declarations: [RelativeTime],
  imports: [],
  exports: [RelativeTime]
})
export class DirectivesModule {}
