export class TokenSessionModel {
    
    currentDtls: {
        _id?: string;
        clientId: string;
        serviceDate: Date;
        session: string;
        currentToken: number;
        status?: string
    };
    aptDtls?: {
        _id?: string,
        clientId?: string,
        serviceForWhom?: string,
        appointmentStatus?: boolean,
        appointmentTime?: string,
        sessionNumber?: string,
        tokenNumber?: number,
        createdAt?: string,
        providerStatus: string,
        providerNotes : string,
        phoneNum?: number,
        name?: string,
        gender?:string
        
    };


    public static createBlank() {
        let blank = {
            currentDtls: {
                clientId: null,
                serviceDate: null,
                session: null,
                currentToken: null
            },
          
        }
        return blank;
    }
}