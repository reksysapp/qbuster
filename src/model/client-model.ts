export class ClientModel {
    _id: string;
    name: string;
    emailId: string;
    phoneNum: string;
    providerType: string; // 'PRIMARY', 'SECONDARY' //
    password: string;
    primaryProvider?: string;
    primaryProviderName?: string;
    primaryProviderPhoneNum?: string;
    deviceId:String;
    providerDetails: {
        tokenOwner: string; //APP/CLIENT
        addressLine1: string;
        addressLine2: string;
        city: string;
        state: string;
        zip: string;
        country: string;
        location?: string;
        providerSector: string;
        website?: String;
        image?: string;

        businessHours?: {
            day: string;
            openTime: string;
            closingTime: string;
            holidayFlag: string
        }[];
    }
    sessionDetails: {
        sessionType: string, //single/multiple/appointment
        tokenPrefix: string,
        tokenSuffix: string,
        sessions: {
            sessionNumber: number;
            startTime: string;
            endTime: string;
            enableMaxToken: Boolean;
            maxToken: string;
            timeInterval: string
        }[];
    }
    website?: string;
    callingId:string;
    callingStatus:string;
    dpImage: {
        contentType: String, filename: String
    }
    profileStatus?: string;
    primaryProviderId?: string;
    providerLoginId?: string;
    memberFields?: {
        _id: string,
        refFieldNo: string,
        refFieldKey: string,
        refFieldValue: string
    }[];

    public static createBlank() {
        let blank = {
            _id: null,
            name: null,
            emailId: null,
            phoneNum: null,
            providerType: "PRIMARY", // 'PRIMARY', 'SECONDARY //
            password: null,
            callingId:"",
            callingStatus:"",
            dpImage: {
                contentType: "jpeg/png", filename: "image-1545908472579"
            },
            deviceId:"",
            providerDetails: {
                tokenOwner: "app", //APP/CLIENT
                addressLine1: null,
                addressLine2: null,
                city: null,
                state: null,
                zip: null,
                country: null,
                providerSector: null,
            },
            sessionDetails: {
                sessionType: null, //SINGLE/MULTIPLE
                tokenPrefix: null,
                tokenSuffix: null,
                sessions: [{
                    sessionNumber: null, startTime: null, endTime: null,
                    maxToken: null, enableMaxToken: false, timeInterval: null
                }]
            },
        }
        return blank;
    }
}

