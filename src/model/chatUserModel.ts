export class ChatUserModel {
    constructor(
        public _id:any,
        public name: string,
        public status?:string,
        public avatar?:any,
        public message?:any,
       
    ) { }

    public static createBlank() {
          let blankUser = {
              _id:null,
            name:""
        }
        return blankUser;
    }
}
