export class AppointmentModel {
  _id: string;
  providerId: string;
  customerId: string;
  customerType: string; /* PRIMARY', 'SECONDARY', 'ONTHESPOT' */
  bookingType: string; /* NEW, RESCHEDULE */
  aptDate: Date;
  sessionType: string;
  sessionNumber: string;
  specialist?: string;
  speciality?: string;
  tokenNumber?: number;
  aptSlot: string;
  customerNotes?: string;
  providerNotes?: string;
  providerStatus?: string;
  tokenValidity?: Date;
  status: Boolean;
  statusReason?: string;
  createdVia?: string; /* MOBILE , WEB */
  createdBy?: string;
  createdAt?: Date;
  checkinFlag?: Boolean;

  public static createBlank() {
    let blank = {
      _id: null,
      providerId: null,
      customerId: null,
      customerType: null,
      bookingType: "NEW",
      aptDate: null,
      sessionType: null,
      sessionNumber: null,
      status: true,
      aptSlot: null
    }
    return blank;
  }


}
