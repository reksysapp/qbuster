export class CustomerModel {
  _id: string;
  emailId: string;
  phoneNum: number;
  password: string;
  name: string;
  gender: string;
  dob: Date;
  addressLine1: string;
  addressLine2: string;
  city: string;
  state: string;
  zip: string;
  country: string;
  hasOwnIdFlag:boolean;
 callingId:string;
 callingStatus:string;
 deviceId:string;
  customerLoginId?: string;
  customerType: string;
  primaryUserId?: string;
  status?: string;
  relationship: string;
  passwordResetToken?: string;
  passwordResetExpires?: Date;
  contactPreference?: string;
  dpImage: {
    contentType: String, filename: String
}
  profileStatus?:string;
  secondaryMemberId?:string;
  secondaryMemberName?:string;

  public static createBlank() {
    let blank = {
      _id: null,
      emailId:null,
      phoneNum : null,
      password: null,
      name: null,
      gender: "None",
     callingId:"",
     callingStatus:"",
     deviceId:"",
      dob : null,
      addressLine1: null,
      addressLine2: null,
      city: null,
      state: null,
      zip: null,
      country: null,
      relationship : "Self",
      customerType : "PRIMARY",
      hasOwnIdFlag:false, 
      dpImage: {
        contentType: "jpeg/png", filename: "image-1545908472579"
    },
    }
    return blank;
  }
}
