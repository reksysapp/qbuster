import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SuperTabs } from 'ionic2-super-tabs';


@IonicPage()
@Component({ 
  selector: 'page-hourly-appoinment',
  templateUrl: 'hourly-appoinment.html',
})
export class HourlyAppoinmentPage {
     
  pages = [    
    { pageName: 'MorningSessionPage', title: 'Morning Session', id: 'Morning'}, 
    { pageName: 'AfternoonSessionPage', title: 'Afternoon Session', id: 'Afternoon'},
    // { pageName: 'AccountPage', title: 'Body', icon: 'body', id: 'accountTab'}
  ]; 
  selectedTab = 0;
 
  @ViewChild(SuperTabs) superTabs: SuperTabs;
 
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
   }
 
}