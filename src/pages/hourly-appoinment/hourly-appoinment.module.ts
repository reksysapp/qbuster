import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HourlyAppoinmentPage } from './hourly-appoinment';
import { SuperTabsModule } from 'ionic2-super-tabs';

@NgModule({
  declarations: [
    HourlyAppoinmentPage,
  ],
  imports: [
    IonicPageModule.forChild(HourlyAppoinmentPage),
    SuperTabsModule 
  ],
})
export class HourlyAppoinmentPageModule {}
