import { ChatUserServiceProvider } from './../../providers/chat-user-service/chat-user-service';
import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CustomerServiceProvider } from './../../providers/customer-service/customer-service';
import { CustomerModel } from '../../model/customer-model';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { ClientModel } from './../../model/client-model';
import { HttpClient } from '@angular/common/http';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { AppointmentServiceProvider } from './../../providers/appointment-service/appointment-service';
import { CustomerMenuPage } from '../customer-menu/customer-menu';
import { ForgotPasswordPage } from './../forgot-password/forgot-password';
import { Storage } from '@ionic/storage';
import { TermsAndConditionPage } from '../terms-and-condition/terms-and-condition';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { Device } from '@ionic-native/device';

@IonicPage()
@Component({
  selector: 'page-customer-login',
  templateUrl: 'customer-login.html',
})
export class CustomerLoginPage {
  customer: CustomerModel;
  client: ClientModel;
  countryCode: string;
  CustomerLoginForm: FormGroup;
  CustomerRegisterForm: FormGroup;
  customerlogin: boolean = true;
  customersignup: boolean = false;
  showPwd: any;
  rootNavCtrl: NavController;
  userId: any;

  options : InAppBrowserOptions = {
    
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
   
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only 
    hidenavigationbuttons:'yes',
    toolbarcolor:'#F6BB42'   
};

  constructor(public storage:Storage,public http: HttpClient, private toast: ToastServiceProvider, public navCtrl: NavController,
    public navParams: NavParams, public customerService: CustomerServiceProvider, private device :Device,private formBuilder: FormBuilder,
    public clientService: ClientServiceProvider, public appointmentService: AppointmentServiceProvider
    , public chatUserService:ChatUserServiceProvider,private theInAppBrowser: InAppBrowser) {

    this.rootNavCtrl = this.navParams.get('rootNavCtrl');
    this.customer = CustomerModel.createBlank();
    this.client = ClientModel.createBlank();

    this.CustomerLoginForm = this.formBuilder.group({
      mobile_no: ['', Validators.compose([Validators.required, Validators.pattern('^(?:[1-9][0-9]{9}|[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})$')])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(10)])],
      country: ['', Validators.compose([Validators.required])]

    });

    this.CustomerRegisterForm = this.formBuilder.group({

      mobile_no: ['', Validators.compose([Validators.required, Validators.pattern('^(?:[1-9][0-9]{9}|[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})$')])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(10)])],
      country: ['', Validators.compose([Validators.required])]

    });

    this.countryCode = '+91';
    // setInterval(() => {
    //   this.socket.emit('deviceId',this.device.uuid)
      
    // },10000)
    
  //   this.socket.on('is_online', (data) => {
  //     console.log("deviceId",this.device.uuid)
  // });

  }
  //Login for User
  doLogin() {
    console.log("login");
    let email = /^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    // Rincy proper coding
    if (this.userId.includes('@')) {
      console.log("is email");
      this.customer.emailId = this.userId.toLowerCase();
      // Rincy proper coding
      if (!email.test(String(this.customer.emailId).toLowerCase())) {
        this.toast.showAlert("Please enter a valid Email-id.")
      }
      else {
        this.loginCustomer();
      }
    }
    else {
      console.log("is phone number")
      this.customer.phoneNum = this.userId;
      this.loginCustomer();
    }

  }
  loginCustomer() {
    this.toast.showLoading("Please Wait ...");
    this.customerService.loginCustomer(this.customer).subscribe(res => {
      this.toast.dismissLoading();
      this.customer = res.body
      this.customer.profileStatus = res.profileStatus;
      this.customerService.setCustomer(this.customer)
      this.storage.set('user', this.customer);
      this.rootNavCtrl.push(CustomerMenuPage)
    }, error => this.onError(error)
    )
  }

  //Function for hiding div for customer
  hideForCustomer() {
    this.customer = CustomerModel.createBlank();
    this.userId = null;
    this.customerlogin = !this.customerlogin;
    this.customersignup = !this.customersignup;
  }

  //Signup for customer
  doSignup() {
    let email = /^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    // Rincy proper coding
    if (this.userId.includes('@')) {
      console.log("is email");
      this.customer.emailId = this.userId.toLowerCase();
      // Rincy proper coding
      if (!email.test(String(this.customer.emailId).toLowerCase())) {
        this.toast.showAlert("Please enter a valid Email-id.")
      }
      else {
        this.SignupCustomer();
      }
    }
    else {
      console.log("is phone number")
      this.customer.phoneNum = this.userId;
      this.SignupCustomer();
    }

  }
  SignupCustomer() {
    this.toast.showLoading("Please Wait ...");
    this.customer.hasOwnIdFlag=true;
    this.customer.deviceId=this.device.uuid
    this.customerService.SignupCustomer(this.customer).subscribe(res => {
      this.toast.presentToast("Registration Successful")
      this.toast.dismissLoading();
      this.customerlogin = true;
      this.customersignup = false;
      this.userId = null;
      this.customer = CustomerModel.createBlank();
    }, error => this.onError(error))
  }
  //For forgot password
  forgot() {
    this.rootNavCtrl.push(ForgotPasswordPage)
  }

  /* on click terms and condition page */
  termspage(){
    this.rootNavCtrl.push(TermsAndConditionPage);
  }

  public openWithInAppBrowser(url : string){
    let target = " _self";
    this.theInAppBrowser.create(url,target,this.options);
}

  //Handles service response error
  onError(error) {
    this.toast.dismissLoading();
    console.log(error);
    this.toast.showError(error);
  }
}
