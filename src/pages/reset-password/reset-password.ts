import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CustomerModel } from '../../model/customer-model';
import { CustomerServiceProvider } from '../../providers/customer-service/customer-service';
import { ClientServiceProvider } from '../../providers/client/client-service';
import { ClientModel } from '../../model/client-model';

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {

  user: FormGroup;
  customer: CustomerModel;
  password: string;
  value: any;
  oldPassword : string;
  provider: ClientModel;

  constructor(private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams,
    public customerService: CustomerServiceProvider, public clientService: ClientServiceProvider) {
    this.customer = this.customerService.getCustomer();
    this.password = null;
    this.provider = this.clientService.getClient();
    this.value = navParams.get("res");
    this.user = this.formBuilder.group({
      oldPassword: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(10)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(10)])],
      confirmPassword: ['']
    }, { validator: this.checkPasswords });
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true }
  }
  savePassword() {


    if (this.value == "PROVIDER") {
      this.provider.password = this.password
      console.log("inside save password", this.provider)
      this.clientService.updatePassword(this.provider).subscribe(res => {
        console.log("inside save password service", res);
        this.navCtrl.pop();
      })
    }
    else {
      this.customer.password = this.password
      console.log("inside save password", this.customer)
      this.customerService.updatePassword(this.customer).subscribe(res => {
        console.log("inside save password service", res);
        this.navCtrl.pop();
      })
    }

  }

}
