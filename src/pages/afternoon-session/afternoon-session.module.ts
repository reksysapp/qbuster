import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AfternoonSessionPage } from './afternoon-session';

@NgModule({
  declarations: [
    AfternoonSessionPage,
  ],
  imports: [
    IonicPageModule.forChild(AfternoonSessionPage),
  ],
})
export class AfternoonSessionPageModule {}
