import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { AppointmentModel } from './../../model/appointment-model';
import { ClientModel } from './../../model/client-model';
import { ProviderAddSpecialistPage } from '../provider-add-specialist/provider-add-specialist';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';

@IonicPage()
@Component({
  selector: 'page-provider-specialists',
  templateUrl: 'provider-specialists.html',
})
export class ProviderSpecialistsPage {
  client: ClientModel;
  appointments: AppointmentModel[];
  specialistDetails: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    public clientService: ClientServiceProvider, public viewCtrl: ViewController,public toast: ToastServiceProvider) {
    this.client = this.clientService.getClient();
    this.specialistDetails = this.navParams.get('secondaryList')
    console.log("specialist list   :", this.specialistDetails[0])
    // Rincy proper coding
    if (!this.specialistDetails[0] || !this.specialistDetails) {
      let profileModal = this.modalCtrl.create(ProviderAddSpecialistPage, { "id": this.client._id });
      profileModal.onDidDismiss(data => {
        // Rincy proper coding
        if (data) {
          this.clientService.getSecondaryProviders(this.client._id).subscribe(res =>{
              this.specialistDetails = res;
          }, error => this.onError(error))
        }
      });
      profileModal.present();
    }
  }

  addSpecialist() {
    let profileModal = this.modalCtrl.create(ProviderAddSpecialistPage, { "id": this.client._id });
    profileModal.onDidDismiss(data => {
      // Rincy proper coding
      if (data) {
    this.clientService.getSecondaryProviders(this.client._id).subscribe(res =>{
        this.specialistDetails = res;
    }, error => this.onError(error))
  }
  });
    profileModal.present()
  }
  dismiss() {
    this.viewCtrl.dismiss(this.specialistDetails);
  }
 //Handles error
 onError(error) {
  this.toast.showError(error);
}
}