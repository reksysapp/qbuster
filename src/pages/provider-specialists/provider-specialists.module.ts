import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderSpecialistsPage } from './provider-specialists';

@NgModule({
  declarations: [
    ProviderSpecialistsPage,
  ],
  imports: [
    IonicPageModule.forChild(ProviderSpecialistsPage),
  ],
})
export class ProviderSpecialistsPageModule {}
