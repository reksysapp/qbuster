import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderChatPage } from './provider-chat';

@NgModule({
  declarations: [
    ProviderChatPage,
  ],
  imports: [
    IonicPageModule.forChild(ProviderChatPage),
  ],
})
export class ProviderChatPageModule {}
