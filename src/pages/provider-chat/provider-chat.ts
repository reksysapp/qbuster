import { CustomerServiceProvider } from './../../providers/customer-service/customer-service';
import { ChatUserModel } from './../../model/chatUserModel';
import { Socket } from 'ngx-socket-io';
import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';
import { ChatUserServiceProvider } from '../../providers/chat-user-service/chat-user-service';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { ClientServiceProvider } from '../../providers/client/client-service';
import { ClientModel } from '../../model/client-model';
import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { ProviderPrivateMessagePage } from '../provider-private-message/provider-private-message';
import { environment } from './../../environment/environment';

@IonicPage()
@Component({
  selector: 'page-provider-chat',
  templateUrl: 'provider-chat.html',
})
export class ProviderChatPage {

  user: ChatUserModel;
  toUser: ChatUserModel[];
  provider: ClientModel;
  imageUrl: any;
  private baseURL = environment.baseURL;

  constructor(public storage: Storage, public event: Events, public chatService: ChatUserServiceProvider, public socket: Socket, public customerService: CustomerServiceProvider,
    public chatUserService: ChatUserServiceProvider, public toast: ToastServiceProvider, public clientService: ClientServiceProvider, public navCtrl: NavController) {
    // Get user information
    this.provider = this.clientService.getClient()
  }

  ionViewWillEnter() {
    this.getMessages();
  }

  public setUser(i) {
    console.log("provider chat",this.toUser[i]);
    this.toUser[i].message.PROVIDER_DEVICE_ID=this.provider.deviceId;
      this.navCtrl.push(ProviderPrivateMessagePage, { "toUser": this.toUser[i] })
    this.socket.connect();
  }

  public getMessages() {
    return this.chatUserService.getProviderMessageList(this.provider).subscribe(
      data => {
        console.log(JSON.stringify(data, null, 2))

        //to store the image url in data array
        for (let i = 0; i < data.length; i++) {
          this.imageUrl = this.baseURL + "customer/getImage/id/" + data[i].message.CUSTOMER_ID;
          data[i].message.image = this.imageUrl;
        }
        this.onMessageSuccess(data)
        return data
      },
      error => this.onError(error)
    );
  }

  onMessageSuccess(msg) {
    this.toUser = msg;
    console.log("chat model is ", JSON.stringify(this.toUser))
    this.toast.dismissLoading();
  }

  onError(error) {
    console.log('Inside OnError.')
    this.toast.dismissLoading();
    console.log(error);
    this.toast.showError(error);
  }
}
