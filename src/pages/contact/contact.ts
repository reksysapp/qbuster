import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  template: `
    <ion-list no-lines>
      <button ion-item (click)="close()">Edit</button>
      <button ion-item (click)="close()">Delete</button>
    </ion-list>
  `
})
export class ContactPage {

  constructor(public navCtrl: NavController) {

  }

}
