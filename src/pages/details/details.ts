import { environment } from './../../environment/environment';
import { ClientSessionPage } from './../client-session/client-session';
import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { AlertController } from 'ionic-angular';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { AppointmentModel } from './../../model/appointment-model';
import { ClientModel } from './../../model/client-model';
import { JsonServicesProvider } from './../../providers/json-services/json-services';
import { ProviderSpecialistsPage } from '../provider-specialists/provider-specialists';
import { ResetPasswordPage } from '../reset-password/reset-password'
import { AddLocationPage } from '../add-location/add-location';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ActionSheetController, normalizeURL } from 'ionic-angular';
import { Crop } from '@ionic-native/crop';

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  countryCode: string;
  client: ClientModel;
  clients: any = [];
  appointments: AppointmentModel[];
  // customer: CustomerModel;
  stateList: any = [];
  citiesList: any = [];
  response: any;
  phoneString: string;
  imageData: any;
  imageUrl: any;
  nullCheck : boolean = false;
  private baseURL = environment.baseURL;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public toast: ToastServiceProvider, public alertCtrl: AlertController,
    public clientService: ClientServiceProvider, public modalCtrl: ModalController,
    public jsonService: JsonServicesProvider, public viewCtrl: ViewController, private camera: Camera,
    public actionSheetCtrl: ActionSheetController, private cdRef: ChangeDetectorRef,
    private crop: Crop) {

    this.countryCode = '+91';
    this.client = this.clientService.getClient();
    console.log("Client ID" + this.client._id)


    //Loads industry list from assets in frontend
    this.jsonService.getIndustryList().subscribe(res => {
      this.clients = res;

    })

    //Loads states list from assets in frontend
    this.jsonService.getStatesList().subscribe(res => {
      this.stateList = res;
      // Rincy proper coding
      if (this.client.providerDetails.state) {
        this.loadCities()
      }

    })
  }
  ionViewDidLoad() {
    this.imageUrl = this.baseURL + "provider/getImage/" + this.client._id + '?decache=' + Math.random();
  }

  //Loads cities list from assets in frontend
  loadCities() {
    this.jsonService.getCitiesList().subscribe(res => {
      this.citiesList = res[this.client.providerDetails.state.toString()]
      if (this.citiesList.indexOf(this.client.providerDetails.city) > -1) {
        this.client.providerDetails.city = this.client.providerDetails.city
      }
      else {
        this.client.providerDetails.city = null;
      }
    })

  }

  passwordChange() {
    console.log("password changed")
    this.response = "PROVIDER";
    this.navCtrl.push(ResetPasswordPage, { "res": this.response });
  }

  addSpecialist() {
    this.nullCheck = true;
    this.phoneString = this.client.phoneNum.toString();
    let email = /^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    if (this.client.phoneNum != null && this.phoneString.length != 10) {
      this.toast.showAlert("Please enter a valid Mobile Number.")
    }
    // Rincy proper coding
    else if (!email.test(String(this.client.emailId).toLowerCase())) {
      this.toast.showAlert("Please enter a valid Email-id.")
    }
    // Rincy proper coding
    else if (this.client.providerDetails.providerSector && this.client.providerDetails.tokenOwner &&
      this.client.providerDetails.city && this.client.providerDetails.state && this.client.name && this.client.name != "" &&
      this.client.providerDetails.zip && this.client.providerDetails.zip != "" &&
      this.client.phoneNum && this.client.phoneNum != "") {

      this.client.emailId = this.client.emailId.toLowerCase();

      //Post request to update the clientDetails
      // sends request as client model
      this.toast.showLoading("");
      this.clientService.updateClient(this.client).subscribe(res => {
        if (this.client.providerDetails.providerSector == "Hospital") {
          this.clientService.getSecondaryProviders(this.client._id).subscribe(res => {
            this.toast.dismissLoading();
            let profileModal = this.modalCtrl.create(ProviderSpecialistsPage, { "secondaryList": res });
            profileModal.present();
          }, error => this.onError(error))

        }
      }, error => this.onError(error))
    }
    else {
      this.toast.showAlert("Please fill all the mandatory fields.")
    }

  }
  //Updates the clientDetails in db
  save() {
    this.nullCheck = true;
    this.phoneString = this.client.phoneNum.toString();
    let email = /^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    if (this.client.phoneNum != null && this.phoneString.length != 10) {
      this.toast.showAlert("Please enter a valid Mobile Number.")
    }
    // Rincy proper coding
    else if (!email.test(String(this.client.emailId).toLowerCase())) {
      this.toast.showAlert("Please enter a valid Email-id.")
    }
    // Rincy proper coding
    else if (this.client.providerDetails.providerSector && this.client.providerDetails.tokenOwner &&
      this.client.providerDetails.city && this.client.providerDetails.state && this.client.name && this.client.name != "" &&
      this.client.providerDetails.zip && this.client.providerDetails.zip != "" &&
      this.client.phoneNum && this.client.phoneNum != "") {

      this.client.emailId = this.client.emailId.toLowerCase();
      //Post request to update the clientDetails
      // sends request as client model
      this.clientService.updateClient(this.client).subscribe(res => {
        console.log("inside updateClient   :")
        this.toast.presentToast("Details updated!")
        this.navCtrl.push(ClientSessionPage)
      }, error => this.onError(error))
    }
    else {
      this.toast.showAlert("Please fill all the mandatory fields.")
    }
  }
  addSession() {
    this.navCtrl.push(ClientSessionPage);
  }

  /* get geoLocation */
  addLocation() {
    let profileModal = this.modalCtrl.create(AddLocationPage);
    profileModal.onDidDismiss(data => {
      // Rincy proper coding
      if (data) {
        console.log("location", data)
        this.client.providerDetails.location = data;

      }
    });
    profileModal.present();
  }

  //Calls when user clicks on image
  //Is to add or update image
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Camera',
          role: 'destructive',
          handler: () => {
            this.takePhoto(1);
            console.log('Camera clicked');
          }
        },
        {
          text: 'Photo Library',
          handler: () => {
            this.takePhoto(0);
            console.log('Archive clicked');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }

  //To handle image which gets by choosing from gallery and from camera click
  takePhoto(sourceType) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: sourceType,
    }


    this.camera.getPicture(options).then((imageData) => {
      console.log("******" + this.imageData)
      this.crop.crop(imageData, { quality: 50, targetWidth: 300 })
        .then(
          cropData => {
            console.log("crop data is ", cropData)
            this.clientService.uploadImage(normalizeURL(cropData), this.client._id).then(res => {
              this.ionViewDidLoad();
              this.cdRef.detectChanges();
              console.log("========" + JSON.stringify(res))
            }).catch(err => {
              console.log("---------" + JSON.stringify(err))
            })
          },
          error => console.error('Error cropping image', error)
        );

    }, (err) => {
      // Handle error
      console.log("error   :", err)
    });
  }

  //Handles error
  onError(error) {
    this.toast.showError(error);
  }
}
