import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderLoginPage } from './provider-login';

@NgModule({
  declarations: [
    ProviderLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(ProviderLoginPage),
  ],
})
export class ProviderLoginPageModule {}
