import { Storage } from '@ionic/storage';
import { ProviderMenuPage } from './../provider-menu/provider-menu';
import { ClientModel } from './../../model/client-model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CustomerModel } from '../../model/customer-model';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { AppointmentServiceProvider } from './../../providers/appointment-service/appointment-service';
import { ForgotPasswordPage } from './../forgot-password/forgot-password';
import { CustomerServiceProvider } from './../../providers/customer-service/customer-service';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { ChatUserServiceProvider } from '../../providers/chat-user-service/chat-user-service';
import { TermsAndConditionPage } from '../terms-and-condition/terms-and-condition';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { Device } from '@ionic-native/device';
@IonicPage()
@Component({
  selector: 'page-provider-login',
  templateUrl: 'provider-login.html',
})
export class ProviderLoginPage {
  customer: CustomerModel;
  client: ClientModel;
  ProviderLoginForm: FormGroup;
  ProviderRegisterForm: FormGroup;
  providerlogin: boolean = true;
  providersignup: boolean = false;
  showPwd: any;
  rootNavCtrl: NavController;
  userId: any;

  options : InAppBrowserOptions = {
    
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
   
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only 
    hidenavigationbuttons:'yes',
    toolbarcolor:'#F6BB42'   
};

  constructor(public storage: Storage, 
    private device: Device,public http: HttpClient, private toast: ToastServiceProvider, public navCtrl: NavController,
    public navParams: NavParams, public customerService: CustomerServiceProvider, private formBuilder: FormBuilder,
    public clientService: ClientServiceProvider, public appointmentService: AppointmentServiceProvider,
    public chatService: ChatUserServiceProvider,private theInAppBrowser: InAppBrowser) {

    this.rootNavCtrl = this.navParams.get('rootNavCtrl');
    this.customer = CustomerModel.createBlank();
    this.client = ClientModel.createBlank();

    this.ProviderLoginForm = this.formBuilder.group({
      mobile_no: ['', Validators.compose([Validators.required, Validators.pattern('^(?:[1-9][0-9]{9}|[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})$')])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(10)])],
      country: ['', Validators.compose([Validators.required])]

    });
    this.ProviderRegisterForm = this.formBuilder.group({

      mobile_no: ['', Validators.compose([Validators.required, Validators.pattern('^(?:[1-9][0-9]{9}|[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})$')])],
      // nickName: ['', Validators.compose([Validators.required])],
      country: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(10)])]

    });
  }

  //Function for hiding div for provider
  hideForProvider() {
    this.client = ClientModel.createBlank();
    this.userId = null;
    this.providerlogin = !this.providerlogin;
    this.providersignup = !this.providersignup;
  }
  //For forgot password
  forgot() {
    this.rootNavCtrl.push(ForgotPasswordPage)
  }
  //Login for provider
  loginClient() {
    let email = /^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    // Rincy proper coding
    if (this.userId.includes('@')) {
      console.log("is email");
      this.client.emailId = this.userId.toLowerCase();
      // Rincy proper coding
      if (!email.test(String(this.client.emailId).toLowerCase())) {

        this.toast.showAlert("Please enter a valid Email-id.")
      }
      else {
        this.SigninClient();
      }
    }
    else {
      console.log("is phone number")
      this.client.phoneNum = this.userId;
      this.SigninClient();
    }

  }
  SigninClient() {
    this.toast.showLoading("Please Wait ...");
    
    this.clientService.SigninClient(this.client).subscribe(res => {
      this.toast.dismissLoading();
      this.client = res.body;
      this.client.profileStatus = res.profileStatus;
      this.clientService.setClient(this.client)
      this.chatService.setUser(this.client)
      this.storage.set('p_user', this.client)
      console.log('Device UUID is: ' + this.device.uuid);

      this.rootNavCtrl.push(ProviderMenuPage);
    }, error => this.onError(error)
    )
  }

  //Signup for provider
  registerClient() {
    this.client.deviceId=this.device.uuid;
  
    let email = /^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    // Rincy proper coding
    if (this.userId.includes('@')) {
      console.log("is email");
      this.client.emailId = this.userId.toLowerCase();
      // Rincy proper coding
      if (!email.test(String(this.client.emailId).toLowerCase())) {

        this.toast.showAlert("Please enter a valid Email-id.")
      }
      else {
        this.SignupClient();
      }
    }
    else {
      console.log("is phone number")
      this.client.phoneNum = this.userId;
      this.SignupClient();
    }

  }
  SignupClient() {
    this.toast.showLoading("Please Wait ...");
   
    this.clientService.SignupClient(this.client).subscribe(res => {
      this.toast.dismissLoading();
      this.toast.presentToast("Registration Successful")
      this.providerlogin = true;
      this.providersignup = false;
      this.userId = null;
      this.client = ClientModel.createBlank();
    }, error => this.onError(error))
  }

   /* on click terms and condition page */
   termspage(){
    this.rootNavCtrl.push(TermsAndConditionPage);
  }

  public openWithInAppBrowser(url : string){
    let target = " _self";
    this.theInAppBrowser.create(url,target,this.options);
}

  //Handles service response error
  onError(error) {
    this.toast.dismissLoading();
    console.log(error);
    this.toast.showError(error);
  }

}
