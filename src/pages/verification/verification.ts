import { ResetPasswordPage } from './../reset-password/reset-password';
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CustomerModel } from './../../model/customer-model';
import { CustomerServiceProvider } from './../../providers/customer-service/customer-service';

@IonicPage()
@Component({
  selector: 'page-verification',
  templateUrl: 'verification.html',
})
export class VerificationPage {
  customer: CustomerModel;
  OtpFilling: FormGroup;
  submitAttempt: boolean = false;
  my_variable: string = "primary";

  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, public customerService: CustomerServiceProvider) {
    this.customer = this.customerService.getCustomer();
    this.OtpFilling = formBuilder.group({
      // otp1: ['', Validators.compose([Validators.maxLength(1), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      otp1: ['', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(1)])],
      otp2: ['', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(1)])],
      otp3: ['', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(1)])],
      otp4: ['', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(1)])],
    });
  }


  save() {

    this.submitAttempt = true;
    this.navCtrl.push(ResetPasswordPage)

  }

  next(el) {
    el.setFocus();
  }
  validation() {
    console.log("clicked")
    this.my_variable = "fp";
  }
}