import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { ClientModel } from './../../model/client-model';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { ProviderMenuPage } from '../provider-menu/provider-menu';

@IonicPage()
@Component({
  selector: 'page-client-session',
  templateUrl: 'client-session.html',
})
export class ClientSessionPage {

  client: ClientModel;
  isToggled: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public clientService: ClientServiceProvider,
    public toast: ToastServiceProvider, public viewCtrl: ViewController) {
    this.isToggled = false;
    this.client = this.clientService.getClient();
    if (!this.client.sessionDetails.sessionType) {
      this.client.sessionDetails.sessions = [{
        sessionNumber: null, startTime: null, endTime: null,
        maxToken: null, enableMaxToken: false, timeInterval: null
      }]
    }
    else if (this.client.sessionDetails.sessionType == 'multiple' && this.client.sessionDetails.sessions.length == 2) {
      this.client.sessionDetails.sessions.push({
        sessionNumber: 2, startTime: null, endTime: null, maxToken: null, enableMaxToken: false, timeInterval: null
      })
    }
  }
  minStartTime(session): string {
    if (this.client.sessionDetails.sessions[session].sessionNumber >= 1) {
      session = session - 1;
      return this.client.sessionDetails.sessions[session].endTime;
    }
  }
  //Null check for mandatory fields To update the session details to client db
  nullChexkofSession() {
    this.client.sessionDetails.sessions[0].sessionNumber = 0

    //Null value check and session updation for single session
    if (this.client.sessionDetails.sessionType == 'single') {

      if (this.client.sessionDetails.sessions[0].startTime &&
        this.client.sessionDetails.sessions[0].endTime) {
        if (this.client.sessionDetails.sessions[0].enableMaxToken && !this.client.sessionDetails.sessions[0].maxToken ||
          this.client.sessionDetails.sessions[0].enableMaxToken && this.client.sessionDetails.sessions[0].maxToken == "") {
          this.toast.showAlert("Max token needed");
        }
        else {

          this.updateSession();
        }

      }
      else {
        this.toast.presentToast("Please fill all the fields")
      }

    }
    else if (this.client.sessionDetails.sessionType == 'multiple') {
      if (this.client.sessionDetails.sessions[0].startTime && this.client.sessionDetails.sessions[0].endTime &&
        this.client.sessionDetails.sessions[1].startTime && this.client.sessionDetails.sessions[1].endTime) {

        if (!this.isToggled && this.client.sessionDetails.sessions.length == 3
          && !this.client.sessionDetails.sessions[2].startTime) {
          if (this.client.sessionDetails.sessions[0].enableMaxToken && !this.client.sessionDetails.sessions[0].maxToken ||
            this.client.sessionDetails.sessions[0].enableMaxToken && this.client.sessionDetails.sessions[0].maxToken == "") {
            this.toast.showAlert("Max token needed for session 1");
          }
          else if (this.client.sessionDetails.sessions[1].enableMaxToken && !this.client.sessionDetails.sessions[1].maxToken ||
            this.client.sessionDetails.sessions[1].enableMaxToken && this.client.sessionDetails.sessions[1].maxToken == "") {
            this.toast.showAlert("Max token needed for session 2");
          }
          else {
            this.client.sessionDetails.sessions.pop()
            this.updateSession();
          }

        }
        else if (this.isToggled && this.client.sessionDetails.sessions.length == 3
          && this.client.sessionDetails.sessions[2].startTime && this.client.sessionDetails.sessions[2].endTime) {
          if (this.client.sessionDetails.sessions[2].enableMaxToken && !this.client.sessionDetails.sessions[2].maxToken ||
            this.client.sessionDetails.sessions[2].enableMaxToken && this.client.sessionDetails.sessions[2].maxToken == "") {
            this.toast.showAlert("Max token needed for session 3");
          }
          else {
            this.updateSession();
          }
        }
        else {
          this.toast.showAlert("Invalid session 3 details !!!!!")
        }

      }
      else {
        this.toast.showAlert("Invalid session details !!!!!")
      }

    }

    else if (this.client.sessionDetails.sessionType == 'appointment') {
      if (this.client.sessionDetails.sessions[0].startTime && this.client.sessionDetails.sessions[0].endTime
        && this.client.sessionDetails.sessions[0].timeInterval) {

        this.updateSession();
      }
      else {
        this.toast.showAlert("Invalid session details")
      }
    }
    else {
      this.toast.showAlert("Select session type")
    }

  }

  //  Service to update session details of cient
  updateSession() {
    if (this.client.sessionDetails.tokenPrefix) {
      this.client.sessionDetails.tokenPrefix = this.client.sessionDetails.tokenPrefix.trim();
    }
    if (this.client.sessionDetails.tokenSuffix) {
      this.client.sessionDetails.tokenSuffix = this.client.sessionDetails.tokenSuffix.trim();
    }
    this.clientService.updateClient(this.client).subscribe(res => {
      this.toast.presentToast("Session details updated!")
      this.navCtrl.setRoot(ProviderMenuPage)
    }, error => this.onError(error))
  }

  //Gets sessions based on session type which the client selected
  session() {
    this.client.sessionDetails.sessions[0].timeInterval = null;
    this.client.sessionDetails.sessions[0].sessionNumber = 0
    if (this.client.sessionDetails.sessionType == 'single') {
      this.client.sessionDetails.sessions.length = 1;
      this.client.sessionDetails.sessions[0].timeInterval = null;
      this.client.sessionDetails.sessions[0].startTime = "08:00";
      this.client.sessionDetails.sessions[0].endTime = "17:00";
      this.client.sessionDetails.sessions[0].maxToken = null;
      this.client.sessionDetails.sessions[0].enableMaxToken = false;
    }
    else if (this.client.sessionDetails.sessionType == 'multiple') {
      this.client.sessionDetails.sessions[0].startTime = null;
      this.client.sessionDetails.sessions[0].timeInterval = null;
      this.client.sessionDetails.sessions[0].endTime = null;
      this.client.sessionDetails.sessions[0].maxToken = null;
      this.client.sessionDetails.sessions[0].enableMaxToken = false;

      this.client.sessionDetails.sessions.push({
        sessionNumber: 1, startTime: null, endTime: null, maxToken: null, enableMaxToken: false, timeInterval: null
      }, {
          sessionNumber: 2, startTime: null, endTime: null, maxToken: null, enableMaxToken: false, timeInterval: null
        })
    }
    else if (this.client.sessionDetails.sessionType == 'appointment') {
      this.client.sessionDetails.sessions.length = 1;
      this.client.sessionDetails.sessions[0].timeInterval = "60";
      this.client.sessionDetails.sessions[0].startTime = "08:00";
      this.client.sessionDetails.sessions[0].endTime = "17:00";
    }
  }

  //Handles error
  onError(error) {
    this.toast.showError(error);
  }

}
