import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientSessionPage } from './client-session';

@NgModule({
  declarations: [
    ClientSessionPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientSessionPage),
  ],
})
export class ClientSessionPageModule {}
