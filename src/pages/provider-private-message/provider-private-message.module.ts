import { ChatUserServiceProvider } from './../../providers/chat-user-service/chat-user-service';
import { RelativeTime } from './../../pipes/relative-time';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmojiPickerComponentModule } from "../../components/emoji-picker/emoji-picker.module";
import { EmojiProvider } from "../../providers/emoji";
import { ProviderPrivateMessagePage } from './provider-private-message';

@NgModule({
  declarations: [
    ProviderPrivateMessagePage,
    RelativeTime
  ],
  imports: [
    EmojiPickerComponentModule,
    IonicPageModule.forChild(ProviderPrivateMessagePage),
  ],
  exports: [
    ProviderPrivateMessagePage
  ],
  providers: [
    ChatUserServiceProvider,
    EmojiProvider
  ]
})
export class ProviderPrivateMessagePageModule {}
