import { ChatUserServiceProvider, UserInfo, ChatMessage } from './../../providers/chat-user-service/chat-user-service';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavParams, NavController } from 'ionic-angular';
import { Events, Content } from 'ionic-angular';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { DirectivesModule } from './../../directives/directives.module';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { VideocallPage } from '../videocall/videocall';

@IonicPage()
@Component({
  selector: 'page-provider-private-message',
  templateUrl: 'provider-private-message.html',
})
export class ProviderPrivateMessagePage {


  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: ElementRef;
  data: any;
  msgList: ChatMessage[] = [];
  user: UserInfo;
  toUser: UserInfo;
  editorMsg = '';
  showEmojiPicker = false;
  leavingChat: boolean;

  constructor(public storage: Storage, public event: Events, navParams: NavParams, public navCtrl: NavController,
    private socket: Socket, private events: Events, public chatUserService: ChatUserServiceProvider,
    public toast: ToastServiceProvider) {
    this.leavingChat = false;
    this.toUser = UserInfo.createBlank();

    console.log("TO USER IN CONS" + JSON.stringify(navParams.get("toUser")))

    this.data = navParams.get("toUser");
    this.chatUserService.getUserData(this.data.message.PROVIDER_ID, this.data.message.PROVIDER_NAME,this.data.message.PROVIDER_DEVICE_ID)
      .then((res) => {
        this.user = res
        console.log("user PARAMS" + JSON.stringify(this.user))
      });

    this.chatUserService.getUserData(this.data.message.CUSTOMER_ID, this.data.message.CUSTOMER_NAME,this.data.message.CUSTOMER_DEVICE_ID)
      .then((res) => {
        this.toUser = res
        console.log("To user Params" + JSON.stringify(this.toUser))
      });

    this.socket.on('output', (data) => {
      console.log("on Output")
      if (data.length) {
        console.log("data length:" + data.length);
        for (var x = 0; x < data.length; x++) {
          this.pushNewMsg(data[x]);
        }
      }
      this.editorMsg = '';
    });

    this.socket.on('disconnect', (data) => {
      if (!this.leavingChat) this.toast.presentToast("Chat-No Internet Connection")
    });
  }
  call(){
    console.log(this.user,this.toUser);
    this.navCtrl.push(VideocallPage,{"from":this.user,"to":this.toUser});
  }
  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    }
  }

  ionViewWillLeave() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'flex';
      });
    }
    this.leavingChat = true;
    // this.socket.disconnect();
  }

  ionViewWillEnter() {
    this.msgList = [];
    console.log("provider will enter starts--------------------");
    this.socket.emit('load', { "user": this.user, "to": this.toUser })
    console.log("provider will enter ends---------------");
  }

  onFocus() {
    this.showEmojiPicker = false;
    this.content.resize();
    this.scrollToBottom();
  }

  switchEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
    if (!this.showEmojiPicker) {
      this.focus();
    } else {
      this.setTextareaScroll();
    }
    this.content.resize();
    this.scrollToBottom();
  }

  sendMsg() {
    if (!this.editorMsg.trim()) return;

    // Mock message
    const id = Date.now().toString();
    let newMsg: ChatMessage = {
      messageId: Date.now().toString(),
      userId: this.user._id, //lupin
      userName: this.user.name,
      userAvatar: this.user.avatar,
      toUserId: this.toUser._id, //clicked rani or anyone
      time: Date.now(),
      message: this.editorMsg,
      status: 'pending'
    };

    if (!this.showEmojiPicker) {
      this.focus();
    }

    this.chatUserService.sendMsg(newMsg)
      .then(() => {
        let index = this.getMsgIndexById(id);
        if (index !== -1) {
          this.msgList[index].status = 'success';
        }
      })
  }

  pushNewMsg(msg: ChatMessage) {
    const userId = this.user._id,
      toUserId = this.toUser._id;


    // Verify user relationships
    if (msg.userId === userId && msg.toUserId === toUserId) {
      this.msgList.push(msg);
    } else if (msg.toUserId === userId && msg.userId === toUserId) {
      this.msgList.push(msg);
    }
  }

  getMsgIndexById(id: string) {
    return this.msgList.findIndex(e => e.messageId === id)
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.isScrolling) {
        this.content.scrollToBottom();
      }
    }, 400)
  }

  private focus() {
    if (this.messageInput && this.messageInput.nativeElement) {
      this.messageInput.nativeElement.focus();
    }
  }

  private setTextareaScroll() {
    const textarea = this.messageInput.nativeElement;
    textarea.scrollTop = textarea.scrollHeight;
  }
}
