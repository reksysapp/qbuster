import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';



@IonicPage()
@Component({
  selector: 'page-add-location',
  templateUrl: 'add-location.html',
})
export class AddLocationPage {
  location: string;
  long: any; //longitude
  lati: any; //latitude
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public geolocation: Geolocation, public toast: ToastServiceProvider, public plt: Platform, public viewCtrl: ViewController) {
    this.location = null;
    this.plt.ready().then(() => {

      //set options..
      var options = {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0
      };
      //get the Current location
      this.geolocation.getCurrentPosition(options).then(data => {
        this.long = data.coords.longitude;
        this.lati = data.coords.latitude;
      }).catch((err) => {
        console.log("Error", err);
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddLocationPage');
  }

  /* on click locate me */
  getLocation() {
      if(!this.lati){
      this.toast.showAlert("Unable to get the location ! Please turn on your GPS and try again ")
    } else {
      this.location = this.lati + ',' + this.long;
    }
  }

  /* on click save */
  save() {
    this.viewCtrl.dismiss(this.location)
  }
  cancel() {
    this.viewCtrl.dismiss(null)
  }

}
