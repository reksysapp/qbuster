import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderAddSpecialistPage } from './provider-add-specialist';

@NgModule({
  declarations: [
    ProviderAddSpecialistPage,
  ],
  imports: [
    IonicPageModule.forChild(ProviderAddSpecialistPage),
  ],
})
export class ProviderAddSpecialistPageModule {}
