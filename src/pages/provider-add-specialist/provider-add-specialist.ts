import { ToastServiceProvider } from './../../providers/toast-service/toast-service';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-provider-add-specialist',
  templateUrl: 'provider-add-specialist.html',
})
export class ProviderAddSpecialistPage {

  specialistDetails: any = [];
  specialistForm: FormGroup;
  memberId: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public toast: ToastServiceProvider,
    public clientService: ClientServiceProvider, private formBuilder: FormBuilder) {

    this.specialistDetails = {
      "_id": null, "providerType": "SECONDARY", "name": null, "gender": null, "phoneNum": null, "emailId": null,
      "memberFields": [{
        "refFieldNo": "1",
        "refFieldKey": "Qualification",
        "refFieldValue": null
      }, {
        "refFieldNo": "2",
        "refFieldKey": "Speciality",
        "refFieldValue": null
      }, {
        "refFieldNo": "3",
        "refFieldKey": "Experience",
        "refFieldValue": null
      }, {
        "refFieldNo": "4",
        "refFieldKey": "Gender",
        "refFieldValue": null
      }]
    };
    this.specialistForm = this.formBuilder.group({

      mobile_no: ['', Validators.compose([Validators.required, Validators.pattern('^(?:[1-9][0-9]{9}|[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})$')])],
      one: ['', Validators.compose([Validators.required])],
      two: ['', Validators.compose([Validators.required])],
      three: ['', Validators.compose([Validators.required])],
      four: ['', Validators.compose([Validators.required])],
      five: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(10)])]

    });
  }
  cancel() {
    this.viewCtrl.dismiss(null);
  }
  save() {
    this.specialistDetails._id = this.navParams.get("id");
    this.specialistDetails.memberFields[1].refFieldValue = this.specialistDetails.memberFields[1].refFieldValue.charAt(0).toUpperCase() + this.specialistDetails.memberFields[1].refFieldValue.substr(1);
    let email = /^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    // Rincy proper coding
    if (this.memberId.includes('@')) {
      console.log("is email");
      this.specialistDetails.emailId = this.memberId.toLowerCase();
      // Rincy proper coding
      if (!email.test(String(this.specialistDetails.emailId).toLowerCase())) {
        this.toast.showAlert("Please enter a valid Email-id.")
      }
      else {
        this.SignupClient();
      }
    }
    else {
      console.log("is phone number")
      this.specialistDetails.phoneNum = this.memberId;
      this.SignupClient();
    }
  }
  SignupClient() {
    this.specialistDetails.memberFields[1].refFieldValue = this.specialistDetails.memberFields[1].refFieldValue.trim();
    this.clientService.SignupClient(this.specialistDetails).subscribe(res => {
      this.viewCtrl.dismiss(res);
    }, err => {
      console.log("err" + err)
      this.toast.showAlert(err)
    })
  }
}
