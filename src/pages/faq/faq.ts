import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {JsonServicesProvider} from '../../providers/json-services/json-services';


@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {
  faq:any =[];
  information: any=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public jsonService: JsonServicesProvider) {
    this.jsonService.getFAQ().subscribe(res => {
      this.information = res;
      console.log("the faq is ",this.information)
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqPage');
  }

  toggleSection(i) {
    this.information[i].open = !this.information[i].open;
  }
 
  toggleItem(i, j) {
    this.information[i].children[j].open = !this.information[i].children[j].open;
  }

}
