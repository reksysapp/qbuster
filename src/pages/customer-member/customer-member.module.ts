import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomerMemberPage } from './customer-member';

@NgModule({
  declarations: [
    CustomerMemberPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomerMemberPage),
  ],
})
export class CustomerMemberPageModule {}
