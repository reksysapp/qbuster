import { CustomerServiceProvider } from './../../providers/customer-service/customer-service';
import { ClientModel } from './../../model/client-model';
import { CustomerAddMemberPage } from './../customer-add-member/customer-add-member';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ViewController } from 'ionic-angular';
import { AppointmentModel } from './../../model/appointment-model';
import { CustomerModel } from '../../model/customer-model';
import { ClientServiceProvider } from '../../providers/client/client-service';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { MemberDetailsPage } from '../member-details/member-details';
@IonicPage()
@Component({
  selector: 'page-customer-member',
  templateUrl: 'customer-member.html',
})
export class CustomerMemberPage {

  client: ClientModel;
  appointments: AppointmentModel[];
  customer: CustomerModel;
  secondaryCustomers: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,public viewCtrl: ViewController,
    public clientService: ClientServiceProvider, public customerService: CustomerServiceProvider, public toast: ToastServiceProvider) {

    this.customer = this.customerService.getCustomer();
    this.client = this.clientService.getClient();
    this.customerService.getSecondaryCustomers(this.customer._id).subscribe(res => {
      console.log("res inside getSecondaryCustomers   :", JSON.stringify(res, null, 2))
      this.secondaryCustomers =res;
      // Rincy proper coding
      if (!this.secondaryCustomers[0]) {
        let profileModal = this.modalCtrl.create(CustomerAddMemberPage, { "id": this.customer._id });
        profileModal.onDidDismiss(data => {
          // Rincy proper coding
          if (data) {
            this.customerService.getSecondaryCustomers(this.customer._id).subscribe(res => {
              this.secondaryCustomers = res
            }, error => this.onError(error))
          }
        });
        profileModal.present();
      }
      console.log("printing members",this.secondaryCustomers)
      // this.navCtrl.push(CustomerMemberPage, { "secondaryList": res })
    }, error => this.onError(error))
  }
  ionViewWillEnter() {
    console.log("secondaryCustomers inside  ionViewWillEnter :", JSON.stringify(this.secondaryCustomers, null, 2))
    // this.secondaryCustomers = this.navParams.get('secondaryList')
  }

  addMember() {
    let profileModal = this.modalCtrl.create(CustomerAddMemberPage, { "id": this.customer._id });
    profileModal.onDidDismiss(data => {
      // Rincy proper coding
      if (data) {
        this.customerService.getSecondaryCustomers(this.customer._id).subscribe(res => {
          this.secondaryCustomers = res
        }, error => this.onError(error))
      }
    });
    profileModal.present()
  }

  details(members) {
    // let DetailModal = this.modalCtrl.create(MemberDetailsPage);
    // DetailModal.present();

    let profileModal = this.modalCtrl.create(MemberDetailsPage,{ "members": members });
    profileModal.present().then(() => {
      this.viewCtrl.dismiss();
    });

    // this.navCtrl.push(MemberDetailsPage, { "members": members });
  }
  //Handles error
  onError(error) {
    this.toast.showError(error);
  }

}
