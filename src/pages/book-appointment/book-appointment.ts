import { AppointmentPage } from './../appointment/appointment';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PincodeSearchPage } from '../pincode-search/pincode-search';

@IonicPage()
@Component({
  selector: 'page-book-appointment',
  templateUrl: 'book-appointment.html',
})
export class BookAppointmentPage {
  tab1Root = AppointmentPage;
  tab2Root = PincodeSearchPage;
  selectedTab = 0;
  reshedule: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log("params inside   :", this.navParams.get('reshedule'))
    let data = this.navParams.get('reshedule')
    if (data) {
      this.reshedule = this.navParams.get('reshedule')
    }
    else {
      this.reshedule = ["new appointment"];
    }
  }

}
