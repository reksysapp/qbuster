import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  options : InAppBrowserOptions = {
    
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
   
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only 
    hidenavigationbuttons:'yes',
    toolbarcolor:'#F6BB42'   
};

  constructor(public navCtrl: NavController,private theInAppBrowser: InAppBrowser) {

  }
  public openWithInAppBrowser(url : string){
    let target = " _self";
    this.theInAppBrowser.create(url,target,this.options);
}





}
