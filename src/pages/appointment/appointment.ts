import { AppointmentModel } from './../../model/appointment-model';
import { AppointmentServiceProvider } from './../../providers/appointment-service/appointment-service';
import { HourlyAppoinmentPage } from './../hourly-appoinment/hourly-appoinment';
import { TokenPage } from './../token/token';
import { ToastServiceProvider } from './../../providers/toast-service/toast-service';
import { ClientModel } from './../../model/client-model';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { JsonServicesProvider } from '../../providers/json-services/json-services';
import { CustomerServiceProvider } from '../../providers/customer-service/customer-service';
import { CustomerModel } from '../../model/customer-model';
import { ModalController } from 'ionic-angular';
import moment from 'moment';
import { WheelSelector } from '@ionic-native/wheel-selector';

@IonicPage()
@Component({
  selector: 'page-appointment',
  templateUrl: 'appointment.html',
})
export class AppointmentPage {

  appointment: AppointmentModel;
  customer: CustomerModel;
  // clients: ClientModel[];
  client: ClientModel;
  statesList: any = [];
  state: any = [];
  city: any = [];
  speciality: any = [];
  specialistArray: any = [];
  specialist: any = [];
  name: any = [];
  citiesList: any = [];
  appointmentTime: any = [];
  appoinments: any = [];
  clientSession: any = [];
  clientSelected: any = [];
  public opt: string = 'signin';
  maxToken: any = [];

  private systemDate = moment().format()
  appointmentDate = new Date();
  resheduledDate = new Date();
  rootNavCtrl: NavController;

  constructor(public navCtrl: NavController, public navParams: NavParams, public customerService: CustomerServiceProvider,
    public clientService: ClientServiceProvider, public jsonService: JsonServicesProvider, public toast: ToastServiceProvider
    , public modalCtrl: ModalController, public viewCtrl: ViewController, public appointmentService: AppointmentServiceProvider
    , private selector: WheelSelector) {

    console.log("inside constructor")
    this.rootNavCtrl = this.navParams.get('rootNavCtrl');
    this.appointment = AppointmentModel.createBlank();
    this.appointment.aptDate = new Date();
    this.appoinments = this.navParams.data;
    this.maxToken = null;
    this.resheduledDate = null;

    //gets the client model from the ClientService Provider
    this.client = this.clientService.getClient();
    this.customer = this.customerService.getCustomer();
    this.client.providerDetails.state = null;
    this.appointment.speciality = null;
    this.appointment.specialist = null;
    /* on reschedule this if condition will execute */
    // Rincy proper coding
    if (this.appoinments && this.appoinments[0] != "new appointment") {
      console.log("inside if  :",this.appoinments[0])
      this.client.providerDetails.state = this.appoinments.state;
      this.loadCities(this.appoinments.state);
    }
    /* else gets executed on new appointment */
    else {
      // gets the states of clients based on the sector selected
      // this.toast.showLoading("");
      console.log("inside if else  :")
      this.clientService.getStates(this.client).subscribe(res => {
        // this.toast.dismissLoading();

        this.state = res

        //Load the client state in the appointment screen
        /* check the list if it has the customer state */
        if (this.state.indexOf(this.customer.state) > -1) {
          this.client.providerDetails.state = this.customer.state
        }
        /* else the the first state will be loaded  */
        else {
          this.client.providerDetails.state = this.state[0]
        }
        this.loadCities(this.client.providerDetails.state);
      }, err => {
        console.log("err" + err)
      })
    }
  }
  minStartDate(): string {
    return this.systemDate;
  }

  minStartTime(): string {
    if (moment(moment(this.appointment.aptDate).format('YYYY-MM-DD')).isAfter(moment(new Date()).format('YYYY-MM-DD'))) {
      return moment(moment(this.appointment.aptDate).format('YYYY-MM-DD')).format();
    }
    return this.systemDate;
  }

  // code for wheel selector to select state
  selectState() {
    let item = [];
    this.state.forEach(element => {
      item.push({ description: element })
    });
    this.selector.show({
      title: "Select State",
      items: [
        item
      ],
      theme: "light",
    }).then(
      result => {
        // this.toast.presentToast(result[0] + ' at index: ' + result[0].index);
        this.client.providerDetails.state = result[0].description;
        this.loadCities(this.client.providerDetails.state)
      },
      err => console.log('Error: ', err)
    );
  }
  // code for wheel selector to select city
  selectCity() {
    let item = [];
    this.city.forEach(element => {
      item.push({ description: element })
    });
    this.selector.show({
      title: "Select City",
      items: [
        item
      ],
      theme: "light",
    }).then(
      result => {
        // this.toast.presentToast(result[0] + ' at index: ' + result[0].index);
        this.client.providerDetails.city = result[0].description;
        this.toast.showLoading("");
        this.loadClientNames(this.client.providerDetails.city)
      },
      err => console.log('Error: ', err)
    );
  }

  loadCities(state) {
    console.log("inside loadCities")
    this.appointment.sessionType = null;
    this.clientSelected = [];
    this.client.providerDetails.state = state;
    this.clientSession = [];
    this.speciality = [];
    this.appointment.speciality = null;
    this.specialist = [];
    this.appointment.specialist = null;
    //reschedule
    //Its based on the data which from reshedule
    //It updates all the details in appoinment page based on the data which want to be resheduled
    // Rincy proper coding
    if (this.appoinments && this.appoinments[0] != "new appointment") {
      if (this.client.providerDetails.state == this.appoinments.state) {
        // this.client = this.appoinments.client;
        this.client.providerDetails.state = this.appoinments.state;
        this.client.providerDetails.city = this.appoinments.city;
        this.client.name = this.appoinments.clientName;
        this.appointment.sessionType = this.appoinments.sessionType;
        this.appointment.sessionNumber = this.appoinments.sessionNum;
        this.clientSession = this.appoinments.clientSession;
        this.appointment.customerId = this.appoinments.customerId;
        this.appointment.providerId = this.appoinments.providerId;
        this.appointment._id = this.appoinments.id;
        this.clientSelected = this.appoinments.client;
        this.resheduledDate = this.appoinments.date;
        this.appointment.aptDate = new Date(this.appoinments.date);
        this.appointment.speciality = this.appoinments.speciality;
        this.appointment.specialist = this.appoinments.specialist;
        this.speciality = this.appoinments.secondary;
        this.specialist = this.appoinments.secondary;
        this.client._id = this.appoinments.providerId;
        this.clientSelected._id = this.appoinments.providerId;
        this.client.phoneNum = this.appoinments.phoneNum;
        this.appointment.customerNotes = this.appoinments.notes;
        // this.loadClientNames(this.client.providerDetails.city)
      }

      else {
        this.client.providerDetails.city = null;
        this.client.name = null;
        this.appointment.sessionType = null;
      }

    }
    /* this will executed during the new appointment */
    else {
      this.city = [];
      this.toast.showLoading("");
      /* gets the city from db by using provider sector and state */
      this.clientService.getCities(this.client).subscribe(res => {
        // this.toast.dismissLoading();
        this.city = res
        //show city in frontend while changing the state
        /* check the list if it has the customer city */
        if (this.city.indexOf(this.customer.city) > -1) {
          this.client.providerDetails.city = this.customer.city
          // this.loadClientNames(this.client.providerDetails.city);
        }
        /* else the the first city will be loaded  */
        else {
          this.client.providerDetails.city = this.city[0]
          this.client.name = null;
          this.appointment.sessionType = null;
          // this.loadClientNames(this.client.providerDetails.city)
        }
        // Rincy proper coding
        this.loadClientNames(this.client.providerDetails.city)
      }, err => this.onError(err))


    }

  }
  loadClientNames(city) {
    console.log("inside loadClientNames")
    this.client.providerDetails.city = city;
    this.clientSelected = [];
    this.appointment.sessionType = null;
    this.clientSession = [];
    this.speciality = [];
    this.appointment.speciality = null;
    this.specialist = [];
    this.appointment.specialist = null;
    this.client.name = null;
    // Rincy proper coding
    if (this.appoinments && this.appoinments[0] != "new appointment") {
      this.client.name = this.appoinments.clientName;
      this.appointment.sessionType = this.appoinments.sessionType;
      this.appointment.sessionNumber = this.appoinments.sessionNum;
      this.clientSelected = this.appoinments.client;
    }
    /* executed during the new appointment */
    else {
     // this.toast.showLoading("");
      /* get provider names by using sector and city */
      this.clientService.getClientNames(this.client).subscribe(res => {
        // console.log("the client name res is ", JSON.stringify(res, null, 2))
        this.toast.dismissLoading();
        this.name = res;


      }, err => this.onError(err))
    }
  }

  // code for wheel selector to select name
  selectName() {
    // let item = [];
    // this.city.forEach(element => {
    //   item.push({description : element})
    // });
    this.selector.show({
      title: "Select Name",
      items: [
        this.name
      ],
      theme: "light",
      displayKey: "name"
    }).then(
      result => {
        // this.toast.presentToast(result[0] + ' at index: ' + result[0].index);
        // this.client.providerDetails.city = result[0].description;
        this.client.name = result[0].name
        this.loadSelectedClient(result[0].index)
      },
      err => console.log('Error: ', err)
    );
  }

  // code for wheel selector to select speciality
  selectSpeciality() {
    let item = [];
    this.specialistArray.forEach(element => {
      item.push({ description: element })
    });
    this.selector.show({
      title: "Select Speciality",
      items: [
        item
      ],
      theme: "light"
    }).then(
      result => {
        // this.toast.presentToast(result[0] + ' at index: ' + result[0].index);
        this.appointment.speciality = result[0].description;
        this.loadSpecialist(this.appointment.speciality)
      },
      err => console.log('Error: ', err)
    );
  }

  // code for wheel selector to select specialist
  selectSpecialist() {
    this.selector.show({
      title: "Select Name",
      items: [
        this.specialist
      ],
      theme: "light",
      displayKey: "name"
    }).then(
      result => {
        // this.toast.presentToast(result[0] + ' at index: ' + result[0].index);
        // this.client.providerDetails.city = result[0].description;
        this.appointment.specialist = result[0].name;
        this.loadSelectedSecondaryProvider(result[0].index)
      },
      err => console.log('Error: ', err)
    );
  }
  loadSelectedClient(client) {
    console.log("inside loadSelectedClient")
    this.speciality = [];
    this.appointment.speciality = null;
    this.specialist = [];
    this.appointment.specialist = null;
    this.clientSession = this.name[client].sessionDetails
    this.appointment.sessionType = this.name[client].sessionDetails.sessionType;
    this.appointment.providerId = this.name[client]._id;
    this.appointment.customerId = this.customer._id;
    this.clientSelected = this.name[client];
    // this.getMaxToken()
    let date = this.appointment.aptDate;
    /* check if the secondary provider exist */
    // Rincy proper coding
    if (this.clientSelected.SecondaryProvider[0]) {
      this.appointment.sessionType = null;
      this.loadSpeciality(this.clientSelected.SecondaryProvider)
    }
    /* max token will be checked if the secondary provider does not exist */
    else {
      this.getMaxToken(date)
    }
  }

  //This method is executed when the city is clicked and
  //it automatically loads the clients details based on city and sector
  loadSpeciality(SecondaryProvider) {
    console.log("inside loadSpeciality")
    this.speciality = [];
    this.appointment.speciality = null;
    this.specialist = [];
    this.appointment.specialist = null;

    this.speciality = SecondaryProvider;
    this.specialistArray = [];

    var obj = {};
    /* get the speciality from specialist array  */
    this.speciality.forEach(element => {
      this.specialistArray.push(element.memberFields[1].refFieldValue);
      console.log("speciality array :", this.specialistArray)
    })

    /* remove the duplicates from specialistArray to display in frontend */
    for (var i = 0; i < this.specialistArray.length; i++)
      obj[this.specialistArray[i]] = this.specialistArray[i]
    this.specialistArray = new Array();
    // Removing duplicates by checking each objects with keys.
    for (var key in obj)
      this.specialistArray.push(obj[key]);
    console.log("speciality new array :", this.specialistArray)
  }

  loadSpecialist(speciality) {
    console.log("inside loadSpecialist    :", speciality, this.speciality)
    this.specialist = [];
    this.appointment.specialist = null;
    /* get the secondary providers array by using speciality which selected*/
    this.speciality.forEach(element => {
      // Rincy proper coding
      if (element.memberFields[1].refFieldValue === speciality) {
        this.specialist.push(element)
        console.log("specialists     :", JSON.stringify(this.specialist, null, 2))
      }
    });

  }


  //It gets when the user selects the client name
  //it loads the details of client based on client selected
  loadSelectedSecondaryProvider(client) {
    console.log("inside loadSelectedSecondaryProvider")
    this.clientSession = this.specialist[client].sessionDetails
    this.appointment.sessionType = this.specialist[client].sessionDetails.sessionType;
    this.appointment.providerId = this.specialist[client]._id
    let secondaryProvider = this.clientSelected;
    this.clientSelected = this.specialist[client];
    this.clientSelected.providerDetails = secondaryProvider.providerDetails;
    this.clientSelected.primaryProviderName = this.client.name;
    this.clientSelected.phoneNum = this.client.phoneNum;
    // this.getMaxToken()
    let date = this.appointment.aptDate
    this.getMaxToken(date)
  }
  //Gets last issued token of selected client from db
  //To disable the session if the max token reached
  getMaxToken(date) {
    console.log("inside getMaxToken")
    if (this.resheduledDate != null) {
      this.appointment.aptDate = date;
      this.resheduledDate = null;
    }
    else {
      this.appointment.aptDate = date;
    }
    // Rincy proper coding
    if (this.appoinments && this.appoinments[0] != "new appointment") {
      this.appointment.providerId = this.appoinments.providerId;
    }
    else {
      this.appointment.providerId = this.clientSelected._id;
    }
    // Rincy proper coding
    if (this.client.name) {
      date = new Date(date.toDateString());
      this.maxToken = null;
      if (this.clientSelected.sessionDetails.sessionType == 'single' || this.clientSelected.sessionDetails.sessionType == 'multiple') {
        // this.toast.showLoading("");
        this.appointmentService.getMaxToken({ "providerId": this.appointment.providerId, "aptDate": date }).subscribe(res => {
          // this.toast.dismissLoading();
          this.maxToken = res
          // return this.maxToken;

        }, error => this.onError(error))
      }
    }
  }

  maxTokenReached() {
    // console.log("inside maxTokenReached")
    // console.log("inside function    :",this.maxToken)
    // Rincy proper coding
    if (this.maxToken && this.appointment.sessionType === 'single') {
      // console.log("max   :",this.maxToken.maxToken,">>>>>>>>>>>>>",this.maxToken.currentToken)
      // if (this.maxToken.sessions[0].enableMaxToken && this.maxToken.sessions[0].maxToken == this.maxToken.sessions[0].currentToken ||
      //   this.maxToken.sessions[0].enableMaxToken && this.maxToken.sessions[0].currentToken > this.maxToken.sessions[0].maxToken) {
      //   return true
      // }
      // else {
      //   return false
      // }
      return (this.maxToken.sessions[0].enableMaxToken && this.maxToken.sessions[0].maxToken == this.maxToken.sessions[0].currentToken ||
        this.maxToken.sessions[0].enableMaxToken && this.maxToken.sessions[0].currentToken > this.maxToken.sessions[0].maxToken)

    } else if (this.maxToken && this.appointment.sessionNumber && this.appointment.sessionType == 'multiple') {
      // console.log("max   :",this.maxToken[this.appointment.sessionNumber].maxToken,">>>>>>>>>>>>>",this.maxToken[this.appointment.sessionNumber].currentToken)
      // if (this.maxToken[this.appointment.sessionNumber].enableMaxToken && this.maxToken[this.appointment.sessionNumber].maxToken == this.maxToken[this.appointment.sessionNumber].currentToken ||
      //   this.maxToken[this.appointment.sessionNumber].enableMaxToken && this.maxToken[this.appointment.sessionNumber].currentToken > this.maxToken[this.appointment.sessionNumber].maxToken) {
      //   return true
      // }
      // else {
      //   return false
      // }
      return (this.maxToken[this.appointment.sessionNumber].enableMaxToken && this.maxToken[this.appointment.sessionNumber].maxToken == this.maxToken[this.appointment.sessionNumber].currentToken ||
        this.maxToken[this.appointment.sessionNumber].enableMaxToken && this.maxToken[this.appointment.sessionNumber].currentToken > this.maxToken[this.appointment.sessionNumber].maxToken)

    }

  }

  //To book the appoinment using given data
  public bookAppointment() {
    /* to store the others name in the model */
    if (!this.customer.secondaryMemberId) {
      this.appointment.customerId = this.customer._id;
    }
    else {
      this.appointment.customerId = this.customer.secondaryMemberId;
      console.log("customer appointment", this.appointment.customerId)
    }
    this.appointment.aptSlot = null;
    this.appointment.customerType = this.customer.customerType;

    /* works during reschedule */
    // Rincy proper coding
    if (!this.appointment.aptDate && this.appoinments.date) {
      this.appointment.aptDate = this.appoinments.date;
    }
    else {
      this.appointment.aptDate = new Date(this.appointment.aptDate.toDateString());
    }

    /* null check that they select the mandatory field */
    // Rincy proper coding
    if (this.appointment.aptDate && this.client.name) {
      // console.log("inside if      :", this.appointment.sessionType, this.appointment.sessionNumber)

      /* check if it is a new appointment */
      // Rincy proper coding
      if (!this.appointment._id) {
        // this.appointment.appointmentTime = null;

        /* check if the session type is single */
        if (this.appointment.sessionType == 'single') {
          this.appointment.sessionNumber = "0";
          this.toast.showLoading("");
          //Post service to get the new appointment
          //Sending the details as appointment  model
          this.appointmentService.bookAppointment(this.appointment).subscribe(res => {
            this.toast.dismissLoading();
            this.client = this.clientSelected;
            this.clientService.setClient(this.client)
            this.rootNavCtrl.push(TokenPage, { "token": res })

          }, error => this.onError(error))
        }
        /* book appointment for the multiple session */
        // Rincy proper coding
        else if (this.appointment.sessionType == 'multiple' && this.appointment.sessionNumber) {

          this.toast.showLoading("");
          this.appointmentService.bookAppointment(this.appointment).subscribe(res => {
            this.toast.dismissLoading();
            this.client = this.clientSelected;
            this.clientService.setClient(this.client)
            this.rootNavCtrl.push(TokenPage, { "token": res })
          }, error => this.onError(error))
        }

        else {
          this.toast.showAlert("Please select a Session!!!!!")
        }
      }
      /* reschedule booking */
      //Post service to get the rescheduled appointment
      // Rincy proper coding
      else if (this.appointment._id) {
        this.toast.showLoading("");
        this.appointment.bookingType = "RESCHEDULE";
        this.appointment.customerId = this.appoinments.customerId;
        this.appointment.customerType = this.appoinments.customerType;
        if (this.appoinments.customerType == "SECONDARY") {
          this.customer.secondaryMemberName = this.appoinments.customerName;
        }
        this.appointmentService.bookAppointment(this.appointment).subscribe(res => {
          this.toast.dismissLoading();
          this.rootNavCtrl.push(TokenPage, { "token": res })

        }, error => this.onError(error))
      }
    }
    else {
      this.toast.showAlert("Invalid Selection!!!!!")
    }
  }

  //Goes to another page if the appointment type is 'appointment'
  // To select the appoinment time
  chooseAppointment() {
    // Rincy proper coding
    if (!this.customer.secondaryMemberId && !this.appointment._id) {
      this.appointment.customerId = this.customer._id;
    }
    // Rincy proper coding
    else if(this.appointment._id){
      this.appointment.customerId = this.appoinments.customerId;
      this.appointment.customerType = this.appoinments.customerType;
      this.appointment.bookingType = "RESCHEDULE";
      this.appointment.providerId = this.appoinments.providerId;
      if (this.appoinments.customerType == "SECONDARY") {
        this.customer.secondaryMemberName = this.appoinments.customerName;
        this.customer.secondaryMemberId = this.appoinments.customerId;
        this.customerService.setCustomer(this.customer)
      }
    }
    else {
      this.appointment.customerId = this.customer.secondaryMemberId;
      console.log("customer id", this.appointment.customerId)
    }
    // Rincy proper coding
    if (!this.appointment.aptDate && this.appoinments.date) {
      this.appointment.aptDate = this.appoinments.date;
    }
    else {
      this.appointment.aptDate = new Date(this.appointment.aptDate.toDateString());
    }
    // if(this.appointment._id != null){
    //   this.appointment.customerId = this.appoinments.customerId;
    //   this.appointment.customerType = this.appoinments.customerType;
    //   this.appointment.bookingType = "RESCHEDULE";
    // }
        
    this.appointmentService.setAppointment(this.appointment)
    this.toast.showLoading("");
    this.appointmentService.appointmentToken(this.appointment).subscribe(res => {
      this.toast.dismissLoading();
      this.client = this.clientSelected;
      this.clientService.setClient(this.client)
      this.rootNavCtrl.push(HourlyAppoinmentPage, { "appointments": res })

    }, error => this.onError(error))

  }

  //Handles error
  onError(error) {
    // console.log('Inside OnError.')
    this.toast.dismissLoading();
    // console.log(error);
    this.toast.showError(error);
  }
}


