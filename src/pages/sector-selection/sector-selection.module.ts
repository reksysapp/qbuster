import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SectorSelectionPage } from './sector-selection';

@NgModule({
  declarations: [
    SectorSelectionPage,
  ],
  imports: [
    IonicPageModule.forChild(SectorSelectionPage),
  ],
})
export class SectorSelectionPageModule {}
