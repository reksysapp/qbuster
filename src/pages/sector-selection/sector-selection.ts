import { BookAppointmentPage } from './../book-appointment/book-appointment';
import { CustomerModel } from './../../model/customer-model';
import { JsonServicesProvider } from './../../providers/json-services/json-services';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { ClientModel } from './../../model/client-model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { CustomerServiceProvider } from '../../providers/customer-service/customer-service';


@IonicPage()
@Component({
  selector: 'page-sector-selection',
  templateUrl: 'sector-selection.html',
})
export class SectorSelectionPage {
  client: ClientModel;
  clients: ClientModel[];
  selectedSector: string;
  customer: CustomerModel;
  sector: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public customerService: CustomerServiceProvider,
    public jsonService: JsonServicesProvider, public clientService: ClientServiceProvider, public viewCtrl: ViewController) {

    this.client = this.clientService.getClient();
    this.sector = navParams.get("res")
    this.customer = this.customerService.getCustomer();

    //gets the client list from the JSON stored in the assets folder(mockIndustry.json).
    this.jsonService.getIndustryList().subscribe(res => {
      this.clients = res;
      console.log(res);
    })
  }


  ionViewWillEnter() {
    this.selectedSector = null
  }
  ionViewWillLeave() {
  }

  //Is to disable the sectors image to avoid the selection of sectors
  // client is the item which comes from html
  // sector is given 

  disable(client) {
    // Rincy proper coding
    return (this.sector.indexOf(client) > -1)

  }

  // Executed when the sector selected from image
  // it gets the item index (or) selected client from client list which loads from json from assets 

  onSectorSelect(clientSector) {
    console.log("before provider select",this.client)
    this.client.providerDetails.providerSector = clientSector;
    this.clientService.setClient(this.client);
    this.navCtrl.push(BookAppointmentPage)
  }

  // Executed when the sector selected from top dropdown
  // it gets the selected client from client list which loads from db 

  onSelect() {
    this.client.providerDetails.providerSector = this.selectedSector;
    this.clientService.setClient(this.client);
    this.navCtrl.push(BookAppointmentPage)
  }

}
