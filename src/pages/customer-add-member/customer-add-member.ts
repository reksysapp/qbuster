import { CustomerServiceProvider } from './../../providers/customer-service/customer-service';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { ToastServiceProvider } from './../../providers/toast-service/toast-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import moment from 'moment';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-customer-add-member',
  templateUrl: 'customer-add-member.html',
})
export class CustomerAddMemberPage {

  memberForm: FormGroup;
  relationsList: any = [];
  members: any = [];
  date = moment().format();
  memberId: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private formBuilder: FormBuilder,
    public toast: ToastServiceProvider, public clientService: ClientServiceProvider, public customerService: CustomerServiceProvider, ) {
    this.members = { "_id": null, "customerType": "SECONDARY", "name": null, "gender": null, "dob": null, "relationship": null, "phoneNum": null, "emailId": null, "hasOwnIdFlag": false, "password": null };
    this.relationsList = ["Father", "Mother", "Son", "Daughter", "Spouse", "Others", "Self"]

    this.memberForm = this.formBuilder.group({

      mobile_no: ['', Validators.compose([Validators.pattern('^(?:[1-9][0-9]{9}|[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})$')])],
      name: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      relation: ['', Validators.compose([Validators.required])],
      dob: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.minLength(4), Validators.maxLength(10)])]

    });
  }
  cancel() {
    this.viewCtrl.dismiss(null);
  }
  save() {
    this.members._id = this.navParams.get("id");
    let email = /^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    if (this.members.hasOwnIdFlag) {
      if (this.memberId) {
        if (this.memberId.includes('@')) {
          console.log("is email");
          this.members.emailId = this.memberId.toLowerCase();
          if (!email.test(String(this.members.emailId).toLowerCase())) {
            this.toast.showAlert("Please enter a valid Email-id.")
          }
          else {
            // this.SignupCustomer();
          }
        }
        else {
          console.log("is phone number")
          this.members.phoneNum = this.memberId;
        }
      }
      else {
        this.toast.showAlert("Please enter your Email-id/Mobile number.")
      }

    } else {
    }
    // Rincy proper coding
    this.SignupCustomer();
  }
  SignupCustomer() {
    this.members.name = this.members.name.charAt(0).toUpperCase() + this.members.name.substr(1);
    if (this.members.hasOwnIdFlag && this.members.password && this.members.password != "") {
      this.customerService.SignupCustomer(this.members).subscribe(res => {
        console.log("inside res")
        this.viewCtrl.dismiss(res);
      }, err => {
        console.log("err" + err)
      })
    }
    else if (!this.members.hasOwnIdFlag) {
      this.customerService.SignupCustomer(this.members).subscribe(res => {
        console.log("inside res")
        this.viewCtrl.dismiss(res);
      }, err => {
        console.log("err" + err)
      })
    }
    else {
      this.toast.showAlert("Please enter the password.")

    }
  }
}
