import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomerAddMemberPage } from './customer-add-member';

@NgModule({
  declarations: [
    CustomerAddMemberPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomerAddMemberPage),
  ],
})
export class CustomerAddMemberPageModule {}
