import { AppointmentServiceProvider } from './../../providers/appointment-service/appointment-service';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { AppointmentModel } from './../../model/appointment-model';
import { CustomerModel } from '../../model/customer-model';
import { CustomerServiceProvider } from '../../providers/customer-service/customer-service';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { AlertController } from 'ionic-angular';
import { ClientModel } from './../../model/client-model';

@Component({
  selector: 'page-appointment-expired',
  templateUrl: 'appointment-expired.html',
})
export class AppointmentExpiredPage {
  client: ClientModel;
  userData: any = [];
  date = new Date();
  appointments: AppointmentModel[];
  customer: CustomerModel;
  rootNavCtrl: NavController;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController,
    public toast: ToastServiceProvider, public alertCtrl: AlertController,
    public clientService: ClientServiceProvider, public customerService: CustomerServiceProvider,
    public appointmentService: AppointmentServiceProvider, public navParams: NavParams) {
    this.rootNavCtrl = this.navParams.get('rootNavCtrl');
  }
  ionViewWillEnter() {
    // Gets appointment list of customer from DB
    this.customer = this.customerService.getCustomer();
    this.client = this.clientService.getClient();
    this.toast.showLoading("");
    //Gets total appointments and its details of the customer by sending customer id
    this.appointmentService.getTokenData(this.customer._id).subscribe(res => {
      this.toast.dismissLoading();
      this.appointments = res;

    }, err => {
      console.log("err" + err)
    })
  }
  //Compares the current date with each appointments date to find 
  // if it is active appointment or expaired appointment
  dateCompare(item) {
    // Rincy null check
    if (((item || {}).aptDate || {})) {
      var varDate = new Date(item.aptDate); //dd-mm-YYYY
      this.date.setHours(0, 0, 0, 0);

      // Rincy proper coding
      return (this.date <= varDate)
    }
    else {
      this.toast.showAlert("Application Problem");
    }
  }

  //Handles error
  onError(error) {
    this.toast.dismissLoading();
    console.log(error);
    this.toast.showError(error);
  }
}


