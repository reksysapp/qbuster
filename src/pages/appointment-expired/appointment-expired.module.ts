import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentExpiredPage } from './appointment-expired';

@NgModule({
  declarations: [
    AppointmentExpiredPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentExpiredPage),
  ],
})
export class AppointmentExpiredPageModule {}
