import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class SplashPage {
  splash = true; 
  constructor(public navCtrl: NavController,public viewCtrl : ViewController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.navCtrl.push(LoginPage)
    }, 4000);
  }

}
