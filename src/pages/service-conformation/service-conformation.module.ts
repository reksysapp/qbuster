import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiceConformationPage } from './service-conformation';

@NgModule({
  declarations: [
    ServiceConformationPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiceConformationPage),
  ],
})
export class ServiceConformationPageModule {}
