import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TokenSessionProvider } from './../../providers/token-session/token-session';
import { TokenSessionModel } from './../../model/token-session';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { ClientModel } from './../../model/client-model';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';

@IonicPage()
@Component({
  selector: 'page-service-conformation',
  templateUrl: 'service-conformation.html',
})
export class ServiceConformationPage {

  client: ClientModel;
  tokenSession: TokenSessionModel;
  providerNotes : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public clientService: ClientServiceProvider,
    public tokenSessionService: TokenSessionProvider, public toast: ToastServiceProvider) {

    this.client = this.clientService.getClient();
    this.tokenSession = this.tokenSessionService.getTokenSession();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServiceConformationPage');
  }
  dismiss(){
  }
  noShow(){
    // Rincy proper coding
    if(!this.tokenSession.aptDtls){
      let req = {"status" : this.tokenSession.currentDtls.status , "tokenNumber" : this.tokenSession.currentDtls.currentToken,
      "customerId":this.tokenSession.currentDtls.currentToken ,"clientId" : this.tokenSession.currentDtls.clientId,
      "serviceDate":this.tokenSession.currentDtls.serviceDate,"session":this.tokenSession.currentDtls.session,
      "providerNotes": this.providerNotes, "providerStatus" : "NOSHOW"}

      this.appointmentServed(req);
    }
    else{
   let req = {"_id":this.tokenSession.aptDtls._id, "providerNotes": this.providerNotes, "providerStatus" : "NOSHOW"}
    this.appointmentServed(req);
  }
  }
  served(){
    // Rincy proper coding
    if(!this.tokenSession.aptDtls){
      let req = {"status" : this.tokenSession.currentDtls.status , "tokenNumber" : this.tokenSession.currentDtls.currentToken,
      "customerId":this.tokenSession.currentDtls.currentToken ,"clientId" : this.tokenSession.currentDtls.clientId,
      "serviceDate":this.tokenSession.currentDtls.serviceDate,"session":this.tokenSession.currentDtls.session,
      "providerNotes": this.providerNotes, "providerStatus" : "SERVED"};
      this.appointmentServed(req);
    }
    else{
   let req = {"_id":this.tokenSession.aptDtls._id, "providerNotes": this.providerNotes, "providerStatus" : "SERVED"}
    this.appointmentServed(req);
    }
  }
  appointmentServed(req){
    this.tokenSessionService.served(req).subscribe(res => {
      this.viewCtrl.dismiss(res);
    }, error => this.onError(error))
  }
  cancel(){
    this.viewCtrl.dismiss();
  }
  onError(error) {
    console.log('Inside OnError.')
    console.log(error);
    this.toast.showError(error);
  }

}
