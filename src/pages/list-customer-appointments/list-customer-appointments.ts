import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ClientModel } from './../../model/client-model';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { ProviderPrivateMessagePage } from '../provider-private-message/provider-private-message';

@IonicPage()
@Component({
  selector: 'page-list-customer-appointments',
  templateUrl: 'list-customer-appointments.html',
})
export class ListCustomerAppointmentsPage {
  public show: string;
  public buttonName: any = 'Details';
  public details: any = [];
  hideme = []
  client: ClientModel;
  listAppointments: any = [];
  getRequestDate: any = [];
  todayDate: Date;
  todayDateString: string;
  todayDateModify: Date;



  constructor(public event: Events, public storage: Storage, public navCtrl: NavController, public navParams: NavParams, public clientService: ClientServiceProvider) {
    this.todayDate = new Date();
    this.todayDateString = new Date(this.todayDate.toDateString()).toString();
    this.todayDateModify = new Date(this.todayDate);
    this.client = this.clientService.getClient();
    this.listAppointments = navParams.get("res");
    console.log("data inside params       :", navParams.get("res"))
    this.getRequestDate = navParams.get("res2");
    console.log("data inside getRequestDate       :", this.getRequestDate)
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ListCustomerAppointmentsPage');
  }

  checkIn(index) {
    /* check in code here */

    this.listAppointments[index].checkinFlag = true
    console.log("checking", this.listAppointments[index])
    const reqData = {
      "_id": this.listAppointments[index]._id,
      "checkinFlag": this.listAppointments[index].checkinFlag
    }
    this.clientService.aptCheckin(reqData).subscribe(res => {
      console.log("checking res", res)
    })

  }
  checkedOut(index) {
    /* check in code here */
    this.listAppointments[index].checkinFlag = false;
    const reqData = {
      "_id": this.listAppointments[index]._id,
      "checkinFlag": this.listAppointments[index].checkinFlag
    }
    this.clientService.aptCheckin(reqData).subscribe(res => {
      console.log("checking res", res)
    })

  }

  public chatToCustomer(i) {
    const requestPayload ={message:{
      "CUSTOMER_ID":this.listAppointments[i].CustomerDocs._id,
      "PROVIDER_ID":this.client._id,
      "PROVIDER_NAME":this.client.name,
      "CUSTOMER_NAME":this.listAppointments[i].CustomerDocs.name
   }}
    this.navCtrl.push(ProviderPrivateMessagePage,{"toUser": requestPayload })
  }



}
