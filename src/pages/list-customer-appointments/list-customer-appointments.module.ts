import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListCustomerAppointmentsPage } from './list-customer-appointments';

@NgModule({
  declarations: [
    ListCustomerAppointmentsPage,
  ],
  imports: [
    IonicPageModule.forChild(ListCustomerAppointmentsPage),
  ],
})
export class ListCustomerAppointmentsPageModule {}
