import { VerificationPage } from './../verification/verification';
import { CustomerModel } from './../../model/customer-model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CustomerServiceProvider } from './../../providers/customer-service/customer-service';

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
  customer: CustomerModel;
  country: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,public customerService: CustomerServiceProvider) {
    this.customer=CustomerModel.createBlank();
    this.country= 'India';
  }
 
  ionViewDidLoad() {
    
  }
  send(){
    
    this.navCtrl.push(VerificationPage)

  }
}
