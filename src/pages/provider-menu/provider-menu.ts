import { LoginPage } from './../login/login';
import { DetailsPage } from './../details/details';
import { ProviderHomePage } from './../provider-home/provider-home';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, ViewController, AlertController } from 'ionic-angular';

export interface MenuItem {
  title: string;
  component: any;
  icon: string;

}

@IonicPage()
@Component({
  selector: 'page-provider-menu',
  templateUrl: 'provider-menu.html',
})
export class ProviderMenuPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = ProviderHomePage;
  appMenuItems: Array<MenuItem>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    private alertCtrl: AlertController) {

    this.appMenuItems = [
      { title: 'Home', component: ProviderHomePage, icon: 'home' },
      { title: 'Details', component: DetailsPage, icon: 'document' }
    ];
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  closeMenu() {
    let alert = this.alertCtrl.create({
      message: 'Do you want to log out?',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
            this.navCtrl.push(LoginPage).then(() => {
              const index = this.viewCtrl.index;
              this.navCtrl.remove(index);
            });
          }
        },        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }
}
