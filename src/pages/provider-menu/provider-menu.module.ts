import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderMenuPage } from './provider-menu';

@NgModule({
  declarations: [
    ProviderMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(ProviderMenuPage),
  ],
})
export class ProviderMenuPageModule {}
