import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProviderHomePage } from './provider-home';

@NgModule({
  declarations: [
    ProviderHomePage,
  ],
  imports: [
    IonicPageModule.forChild(ProviderHomePage),
  ],
})
export class ProviderHomePageModule {}
