import { ProviderChatPage } from './../provider-chat/provider-chat';
import { ListCustomerAppointmentsPage } from './../list-customer-appointments/list-customer-appointments';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { TokenSessionModel } from './../../model/token-session';
import { TokenSessionProvider } from './../../providers/token-session/token-session';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController, MenuController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { ClientModel } from './../../model/client-model';
import { ServeTokenPage } from '../serve-token/serve-token';
import { DetailsPage } from '../details/details';
import { LoginPage } from './../login/login';


@IonicPage()
@Component({
  selector: 'page-provider-home',
  templateUrl: 'provider-home.html',
})
export class ProviderHomePage {
  @ViewChild(Slides) slides: Slides;
  client: ClientModel;
  tokenSession: TokenSessionModel;
  slide: any[];
  testRadioOpen = false;
  testRadioResult: any;
  selectedDate: Date;
  constructor(private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams,
    public clientService: ClientServiceProvider, public tokenSessionService: TokenSessionProvider,
    public toast: ToastServiceProvider, public viewCtrl: ViewController, public menuCtrl: MenuController) {

    this.selectedDate = null;
    this.client = this.clientService.getClient();
    console.log("the client details is ", JSON.stringify(this.client, null, 2))
    this.tokenSession = TokenSessionModel.createBlank();
    this.slide = [
      {
        image: "assets/imgs/queue.jpg",
      },
      {
        image: "assets/imgs/image_in_hand2.jpg",
      }
    ]

    //Checks if the provider filled all the fields
    if (this.client.profileStatus == "INCOMPLETE") {
      this.checkProfile();
    }
  }

  providerChat() {
    this.navCtrl.push(ProviderChatPage)
  }

  ionViewDidLoad() {
  }
  ionViewDidEnter() {
    this.slides.startAutoplay();
    this.slides.autoplayDisableOnInteraction = false;
  }
  checkProfile() {

    let alert = this.alertCtrl.create({
      message: 'Please update your Profile!!!!',
      buttons: [
        {
          text: 'Ok',
          handler: () => {

            this.navCtrl.push(DetailsPage);
          }
        }
      ]
    });
    alert.present();
  }

  serveToken() {
    this.navCtrl.push(DetailsPage)
    //Checks if the provider filled all the fields
  //   if (this.client.profileStatus == "INCOMPLETE") {
  //     this.checkProfile();
  //   }
  //   else if (this.client.sessionDetails.sessionType == 'single') {
  //     let date = new Date();
  //     date = new Date(date.toDateString());
  //     let startSessionReq = { "providerId": this.client._id, "session": "0", "date": date }
  //     this.startSession(startSessionReq);

  //   }
  //   // FIXME- do we need null check here
  //   else if (this.client.sessionDetails.sessionType == 'multiple') {

  //     //Creating the alert to choose the session
  //     let options = {
  //       title: 'Select Session',
  //       buttons: [
  //         {
  //           text: 'Ok',
  //           handler: data => {
  //             if (data) {
  //               let date = new Date();
  //               date = new Date(date.toDateString());
  //               let startSessionReq = { "providerId": this.client._id, "session": data, "date": date }
  //               this.startSession(startSessionReq);
  //             }
  //             else {
  //               this.toast.showAlert("Please choose the session!!!!")
  //             }
  //           }
  //         },
  //         {
  //           text: 'Cancel',
  //           role: 'cancel',
  //           handler: () => {
  //           }
  //         }
  //       ],
  //       inputs: []
  //     };

  //     options.inputs = [];

  //     // Now we add the radio buttons
  //     for (let i = 0; i < this.client.sessionDetails.sessions.length; i++) {
  //       options.inputs.push({
  //         name: 'options', value: this.client.sessionDetails.sessions[i].sessionNumber,
  //         label: " Session " + (i + 1) + "  (" + (this.client.sessionDetails.sessions[i].startTime + "-" + this.client.sessionDetails.sessions[i].endTime) + ")", type: 'radio'
  //       });
  //     }

  //     // Create the alert with the options
  //     let alert = this.alertCtrl.create(options);
  //     alert.present();
  //   }
  // }
  // apt() {
  //   this.navCtrl.push(ListCustomerAppointmentsPage)
  // }
  // startSession(startSessionReq) {
  //   this.tokenSessionService.startSession(startSessionReq).subscribe(res => {
  //     this.tokenSession = res;
  //     this.tokenSessionService.setTokenSession(this.tokenSession)
  //     this.navCtrl.push(ServeTokenPage)
  //   }, error => this.onError(error))
  }

  getDate(date) {
    console.log(date);
    this.selectedDate = date;
    console.log("the lenthent of session", this.client.sessionDetails.sessions.length)
    if (this.client.sessionDetails.sessions.length > 1) {
      this.doRadio();
    }
    else {
      const reqData = {
        "providerId": this.client._id,
        "sessionNumber": "0",
        "aptDate": this.selectedDate
      }
      this.clientService.aptInfo(reqData).subscribe(res => {
        this.navCtrl.push(ListCustomerAppointmentsPage, { "res": res, "res2": reqData });
      }, error => this.onError(error))
    }
  }

  doRadio() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Select session');
    console.log("session length ", this.client.sessionDetails.sessions.length)
    for (let i = 0; i < this.client.sessionDetails.sessions.length; i++) {
      alert.addInput({
        type: 'radio',
        label: this.client.sessionDetails.sessions[i].startTime + " - " + this.client.sessionDetails.sessions[i].endTime,
        value: this.client.sessionDetails.sessions[i].sessionNumber + "",

      });
    }

    alert.addButton({
      text: 'Ok',
      handler: (data: any) => {
        console.log('Radio data:11' + data + "1111");
        this.testRadioOpen = false;
        this.testRadioResult = data;
        this.selectedDate = new Date(this.selectedDate.toDateString());
        const reqData = {
          "providerId": this.client._id,
          "sessionNumber": this.testRadioResult,
          "aptDate": this.selectedDate
        }
        console.log("the request is ", reqData)
        this.clientService.aptInfo(reqData).subscribe(res => {
          this.navCtrl.push(ListCustomerAppointmentsPage, { "res": res, "res2": reqData })
        }, error => this.onError(error))
      }
    });
    alert.addButton('Cancel');


    alert.present();
  }

  closeMenu() {
    let alert = this.alertCtrl.create({
      message: 'Do you want to log out?',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
            this.navCtrl.push(LoginPage).then(() => {
              this.menuCtrl.enable(false);


            })
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  onError(error) {
    console.log(error);
    this.toast.showError(error);
  }
}

