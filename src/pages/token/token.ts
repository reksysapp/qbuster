import { Storage } from '@ionic/storage';
import { BookAppointmentPage } from './../book-appointment/book-appointment';
import { CustomerServiceProvider } from './../../providers/customer-service/customer-service';
import { ClientModel } from './../../model/client-model';
import { AppointmentServiceProvider } from './../../providers/appointment-service/appointment-service';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Nav, AlertController } from 'ionic-angular';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { CustomerModel } from '../../model/customer-model';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { PrivateMessagePage } from '../private-message/private-message';
import { Device } from '@ionic-native/device';

@IonicPage()
@Component({
  selector: 'page-token',
  templateUrl: 'token.html',
})
export class TokenPage {
  @ViewChild(Nav) nav: Nav;
  customer: CustomerModel;
  client: ClientModel;
  token: any = [];
  currentToken: any = [];
  root = this;
  reload: number;
  destination: any;
  currentLocation: any;
  long: any; //longitude
  lati: any; //latitude
  user: any
  start: string;

  constructor(public storage: Storage, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public clientService: ClientServiceProvider, public appointmentService: AppointmentServiceProvider,
    public customerService: CustomerServiceProvider, public toast: ToastServiceProvider,private device:Device,
    public alertCtrl: AlertController, public geolocation: Geolocation, public plt: Platform,
    private launchNavigator: LaunchNavigator) {

    this.token = navParams.get("token")
    this.client = this.clientService.getClient();
    this.customer = this.customerService.getCustomer();

    console.log(this.client)
    console.log(this.token)
    //To initialize the ToUser for the chat(get his details from the provider table)
    this.storage.set('toUser', this.client)

    this.destination = this.client.providerDetails.location;
    console.log("this.destination", this.destination)
    this.plt.ready().then(() => {

      //set options..
      var options = {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0
      };
      //use the geolocation to get the current position of the user
      this.geolocation.getCurrentPosition(options).then(data => {
        this.long = data.coords.longitude;
        this.lati = data.coords.latitude;
        this.start = this.lati + ',' + this.long;
      }).catch((err) => {
        console.log("Error", err);
      });
    });
  }
  ionViewWillEnter(){
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    }
  }

  chat(){

    console.log("push chat page from token")
    console.log("this.token",this.token)
    console.log("this.client",this.client)
    console.log("this.customer",this.customer)
    const requestPayload ={message:{
      "CUSTOMER_ID":this.token.customerId,
      "PROVIDER_ID":this.token.providerId,
      "PROVIDER_NAME":this.client.name,
      "CUSTOMER_NAME":this.customer.name,
      "CUSTOMER_DEVICE_ID":this.device.uuid,
      "PROVIDER_DEVICE_ID":"",
   }}
    this.navCtrl.push(PrivateMessagePage,{"toUser": requestPayload })
  }
  //loads the navigator with both start and destination locations
  getLocation() {
    if (!this.client.providerDetails.location) {
      this.toast.showAlert("Not available for this provider");
    }
    else {
      let options: LaunchNavigatorOptions = {
        start: this.start
      };

      this.launchNavigator.navigate(this.destination, options)
        .then(
          success => alert('Launched navigator'),
          error => alert('unable to launch navigator: ')
        );
    }
  }

  //To remove tab while opening the page
  //The page will display as model
  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    }
  }

  //To view tab while closing the page
  ionViewWillLeave() {
    this.customer.secondaryMemberName = null;
    this.customer.secondaryMemberId = null;
    this.customerService.setCustomer(this.customer)
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'flex';
      });

    }
  }

  //To dismiss or close the page
  dismiss() {
    this.customer.secondaryMemberName = null;
    this.navCtrl.parent.select(0);
  }

  delete() {
    let alert = this.alertCtrl.create({
      // title: 'Confirm purchase',
      message: 'Do you want to cancel the appointment?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            // this.toast.showLoading("");
            this.appointmentService.deleteAppoinment(this.token._id).subscribe(res => {
              this.navCtrl.parent.select(0);
            }, error => this.onError(error))
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  reshedule() {
    let alert = this.alertCtrl.create({
      // title: 'Confirm purchase',
      message: 'Do you want to reshedule the appointment?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            console.log("item inside reshedule    :", JSON.stringify(this.token, null, 2))

            //Passing the resheduled appointment details as params
            //FIX ME -- we write switch insted of if here
            /* varun null check */
            // Rincy null check not needed here
            if (this.client.providerType == "PRIMARY") {
              this.navCtrl.push(BookAppointmentPage, {
                "reshedule": {
                  "state": this.client.providerDetails.state,
                  "city": this.client.providerDetails.city, "clientName": this.client.name,
                  "sector": this.client.providerDetails.providerSector, "customerId": this.token.customerId, "providerId": this.token.providerId,
                  "client": this.client, "phoneNum": this.client.phoneNum,
                  "sessionType": this.client.sessionDetails.sessionType, "sessionNum": this.token.sessionNumber,
                  "clientSession": this.client.sessionDetails, "id": this.token._id, "date": this.token.aptDate,
                  "speciality": null, "specialist": null, "secondary": null, "customerType": this.token.customerType,"customerName": this.customer.secondaryMemberName
                  ,"notes":this.token.customerNotes
                }
              })
            }
            if (this.client.providerType == "SECONDARY") {
              this.navCtrl.push(BookAppointmentPage, {
                "reshedule": {
                  "state": this.client.providerDetails.state,
                  "city": this.client.providerDetails.city, "clientName": this.client.primaryProviderName,
                  "sector": this.client.providerDetails.providerSector, "customerId": this.token.customerId, "providerId": this.token.providerId,
                  "client": this.client, "phoneNum": this.client.phoneNum,
                  "sessionType": this.client.sessionDetails.sessionType, "sessionNum": this.token.sessionNumber,
                  "clientSession": this.client.sessionDetails, "id": this.token._id, "date": this.token.aptDate,
                  "speciality": this.client.memberFields[1].refFieldValue, "specialist": this.client.name, "secondary": [this.client], 
                  "customerType": this.token.customerType,"customerName": this.customer.secondaryMemberName,"notes":this.token.customerNotes
                }
              })
            }
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  call() {
    // Rincy null check
    if(this.client.phoneNum){
    let alert = this.alertCtrl.create({
      // title: 'Confirm purchase',
      message: 'Do you want to connect with provider?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            window.open("tel:" + this.client.phoneNum);
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }
  else{
    this.toast.showAlert("Call not available for this provider")
  }
  }

  navigation() {
    this.getLocation();
  }
  onError(error) {
    // this.toast.dismissLoading();
    console.log(error);
    this.toast.showError(error);
  }

}
