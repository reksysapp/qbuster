import { Storage } from '@ionic/storage';
import { CustomerServiceProvider } from './../../providers/customer-service/customer-service';
import { ChatUserModel } from './../../model/chatUserModel';
import { Socket } from 'ngx-socket-io';
import { Component } from '@angular/core';
import { IonicPage, Events, ViewController, NavController } from 'ionic-angular';
import { ChatUserServiceProvider, UserInfo } from '../../providers/chat-user-service/chat-user-service';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { CustomerModel } from '../../model/customer-model';
import { ClientServiceProvider } from '../../providers/client/client-service';
import { PrivateMessagePage } from '../private-message/private-message';
import { environment } from './../../environment/environment';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  user: ChatUserModel;
  toUser: ChatUserModel[];
  customer: CustomerModel;
  imageUrl: any;
  private baseURL = environment.baseURL;

  constructor(public storage: Storage, public event: Events, public chatService: ChatUserServiceProvider, public socket: Socket, public customerService: CustomerServiceProvider,
    public chatUserService: ChatUserServiceProvider, public toast: ToastServiceProvider, public clientService: ClientServiceProvider, public viewCtrl: ViewController,
    public navCtrl: NavController) {
    // Get user information
    this.customer = this.customerService.getCustomer();
  }

  ionViewWillEnter() {
    this.getMessages();
  }

  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    }
  }

  //To view tab while closing the page
  ionViewWillLeave() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'flex';
      });
    }
  }

  public setUser(i) {
    this.navCtrl.push(PrivateMessagePage, {"toUser": this.toUser[i] })
    this.socket.connect();
  }

  public getMessages() {
    return this.chatUserService.getCustomerMessageList(this.customer).subscribe(
      data => {
        //to store the image url in data array
        for (let i = 0; i < data.length; i++) {
          this.imageUrl = this.baseURL + "provider/getImage/" + data[i].message.PROVIDER_ID;
          data[i].message.image = this.imageUrl;
        }
        this.onMessageSuccess(data)
        return data
      },
      error => this.onError(error)
    );
  }

  onMessageSuccess(msg) {
    this.toUser = msg;
    this.toast.dismissLoading();
  }

  onError(error) {
    console.log('Inside OnError.')
    this.toast.dismissLoading();
    console.log(error);
    this.toast.showError(error);
  }
}
