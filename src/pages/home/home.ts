import { SectorSelectionPage } from './../sector-selection/sector-selection';
import { AppointmentServiceProvider } from './../../providers/appointment-service/appointment-service';
import { ProfilePage } from './../profile/profile';
import { ViewChild } from '@angular/core';
import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { CustomerServiceProvider } from '../../providers/customer-service/customer-service';
import { CustomerModel } from '../../model/customer-model';
import { isBefore } from 'date-fns';
import { AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { ClientModel } from './../../model/client-model';
import { LoginPage } from './../login/login';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { Storage } from '@ionic/storage';


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    @ViewChild(Slides) slides: Slides;
    slide: any[];
    upcomingToken: String;
    todaysTkn: String;
    customer: CustomerModel;
    client: ClientModel;
    todaysToken = [];
    upcoming = [];
    // userType: string;
    profileStatus: string;
    testRadioOpen = false;
    testRadioResult: any;
    secondaryMembers: any = [];
    constructor(public navCtrl: NavController, public customerService: CustomerServiceProvider,
        public alertCtrl: AlertController, public modalCtrl: ModalController, public toast: ToastServiceProvider
        , public clientService: ClientServiceProvider, public viewCtrl: ViewController, public appointmentService: AppointmentServiceProvider,
        public menuCtrl: MenuController,public storage :Storage) {

        this.customer = this.customerService.getCustomer();
        this.client = ClientModel.createBlank();
        // this.userType = this.customer.customerType;
        this.slide = [
            {
                image: "assets/imgs/queue.jpg"
            },
            {
                image: "assets/imgs/time.png"
            },
            {
                image: "assets/imgs/fly.jpg"
            }
        ]

        //To check if the user updated his/her profile completely
        console.log("customer model", JSON.stringify(this.customer, null, 2));

    }

    ionViewWillLeave() {
        this.slides.stopAutoplay();
    }
    ionViewWillEnter() {
       
        let date = new Date();
        date = new Date(date.toDateString());
        let req = { "_id": this.customer._id, "aptDate": date }
        //To get total appointments data To show in frond-end
        //as todays tokens and upcomming tokens
        this.appointmentService.getTotalTokenData(req).subscribe(res => {
            this.todaysToken = [];
            this.upcoming = [];

            //To get todays and upcomming token counts in view token field
            for (let i = 0; i < res.length; i++) {
                let today = new Date()
                if (isBefore(today, new Date(res[i].AppointmentDoc.aptDate))) {

                    this.upcoming.push(res[i])
                }
                else {
                    this.todaysToken.push(res[i])

                }
            }
        }, err => {
            console.log("error   :", err)
        })
        //  // Rincy alert issue fixed "alert not getting after others booking"
        //  this.customer.customerType = this.userType;
        // Rincy alert issue fixed "alert not getting after others booking"
        this.storage.get('user').then((val) => {
            console.log('customerType is', val.customerType);
            this.customer.customerType = val.customerType
          });
    }
    ionViewDidEnter() {
        this.slides.startAutoplay();
        this.slides.autoplayDisableOnInteraction = false;
    }
    /* to check the user has completed his profile */
    checkProfile() {

        let alert = this.alertCtrl.create({
            message: 'Please update your Profile!!!!',
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {

                        this.navCtrl.push(ProfilePage)
                    }
                }
            ]
        });
        alert.present();
    }

    getToken() {
        /* this if is temporary need to clean  */
        // // Rincy alert issue fixed "alert not getting after others booking"
        // this.customer.customerType = this.userType;
        // console.log("customer type   :", this.customer.customerType)
        if (this.customer.customerType == "PRIMARY") {

            let alert = this.alertCtrl.create({
                title: 'Select Person',
                message: 'For whom you want to get the token?',
                buttons: [
                    {
                        text: 'Myself',
                        handler: () => {

                            //To check if the user updated his/her profile completely
                            if (this.customer.profileStatus == "INCOMPLETE" && this.customer.customerType != "SECONDARY") {

                                this.checkProfile();
                            } else {
                                //Gets the sectors in which the providers registered to display in client sector selection page
                                //And passing the response in params
                                this.clientService.getSectors().subscribe(res => {
                                    this.navCtrl.push(SectorSelectionPage, { "res": res })

                                }, err => {
                                    console.log("err" + err)
                                })
                            }

                        }
                    },
                    {
                        text: 'Others',
                        // cssClass: 'alertCustomCss',

                        handler: () => {
                            const reqData = {
                                "_id": this.customer._id
                            }
                            this.customerService.getSecondaryMembers(reqData).subscribe(res => {
                                console.log("response of secondary members is", res)
                                this.secondaryMembers = res;
                                console.log("seondary members is ", JSON.stringify(this.secondaryMembers, null, 2))
                                /* other null check  */
                                if (this.secondaryMembers.length < 1) {
                                    this.navCtrl.push(ProfilePage);
                                    this.toast.presentToast("Kindly Add members...")
                                } else {

                                    let alert = this.alertCtrl.create();
                                    alert.setTitle('Select Member');
                                    for (let i = 0; i < this.secondaryMembers.length; i++) {
                                        alert.addInput({
                                            type: 'radio',
                                            label: this.secondaryMembers[i].name,
                                            value: this.secondaryMembers[i]

                                        });
                                    }


                                    alert.addButton({
                                        text: 'Ok',
                                        handler: (data: any) => {
                                            console.log('Radio data:', data);
                                            this.testRadioOpen = false;
                                            this.testRadioResult = data;
                                            this.customer.secondaryMemberName = data.name;
                                            this.customer.secondaryMemberId = data._id;
                                            this.customer.customerType = "SECONDARY"
                                            this.customerService.setCustomer(this.customer)
                                            this.clientService.getSectors().subscribe(res => {
                                                this.navCtrl.push(SectorSelectionPage, { "res": res })

                                            }, err => {
                                                console.log("err" + err)
                                            })

                                        }
                                    });
                                    alert.addButton('Cancel');

                                    alert.present();
                                }
                            }, error => this.onError(error))


                        }

                    }
                ]
            });
            alert.present();
        }
        else {
            //Gets the sectors in which the providers registered to display in client sector selection page
            //And passing the response in params
            this.clientService.getSectors().subscribe(res => {
                this.navCtrl.push(SectorSelectionPage, { "res": res })

            }, err => {
                console.log("err" + err)
            })
        }

    }
    viewToken() {
        if (this.customer.profileStatus == "INCOMPLETE" && this.customer.customerType != "SECONDARY") {

            this.checkProfile();
        } else {
            this.navCtrl.parent.select(1)
        }

    }
    /* logout */
    closeMenu() {

        let alert = this.alertCtrl.create({
            // title: 'Confirm purchase',
            message: 'Do you want to log out?',
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {
                        console.log('Ok clicked');

                        this.navCtrl.parent.parent.setRoot(LoginPage).then(() => {
                            this.menuCtrl.enable(false);
                        })

                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        alert.present();
    }
    onError(error) {
        // console.log('Inside OnError.')
        this.toast.dismissLoading();
        console.log(error);
        this.toast.showError(error);
    }
}

