import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PincodeSearchPage } from './pincode-search';

@NgModule({
  declarations: [
    PincodeSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(PincodeSearchPage),
  ],
})
export class PincodeSearchPageModule {}
