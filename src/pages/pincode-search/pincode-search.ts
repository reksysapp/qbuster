import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { JsonServicesProvider } from '../../providers/json-services/json-services';
import { ToastServiceProvider } from './../../providers/toast-service/toast-service';

@IonicPage()
@Component({
  selector: 'page-pincode-search',
  templateUrl: 'pincode-search.html',
})
export class PincodeSearchPage {

  zipCode: string;    //variable to store the boolean in toggle method.
  zipState: string = null;
  zipCity: string = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public jsonService: JsonServicesProvider, public toast: ToastServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PincodeSearchPage   :',this.navParams.data);
  }
 //This method is used then the zipcode is selected to load the city state automatically.
 public autoLoadCities() {
  this.jsonService.loadCitiesBasedZip(this.zipCode).subscribe(res => {
    this.zipState = res.PostOffice[0].State;
    this.zipCity = res.PostOffice[0].District
  }, error => this.onError(error))
}
 //Handles error
 onError(error) {
  // console.log('Inside OnError.')
  this.toast.dismissLoading();
  // console.log(error);
  this.toast.showError(error);
}
}
