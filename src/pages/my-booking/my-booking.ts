import { AppointmentExpiredPage } from './../appointment-expired/appointment-expired';
import { AppointmentActivePage } from './../appointment-active/appointment-active';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SuperTabs } from 'ionic2-super-tabs';
import{ MenuController } from 'ionic-angular';


@Component({
  selector: 'page-my-booking',
  templateUrl: 'my-booking.html',
})
export class MyBookingPage {
  tab1Root = AppointmentActivePage;
  tab2Root = AppointmentExpiredPage;
  selectedTab = 0;
 
  @ViewChild(SuperTabs) superTabs: SuperTabs;
 
  constructor(public navCtrl: NavController, public navParams: NavParams,public  menu: MenuController) {
   }
 
}