import { AppointmentServiceProvider } from './../../providers/appointment-service/appointment-service';
import { TokenPage } from './../token/token';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { CustomerServiceProvider } from '../../providers/customer-service/customer-service';
import { CustomerModel } from '../../model/customer-model';
import { ClientModel } from './../../model/client-model';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { AppointmentModel } from '../../model/appointment-model';
import { ToastServiceProvider } from './../../providers/toast-service/toast-service';
import moment from 'moment';
import { LocalNotifications } from '@ionic-native/local-notifications';


@IonicPage()
@Component({
    selector: 'page-morning-session',
    templateUrl: 'morning-session.html',
})
export class MorningSessionPage {

    rootNavCtrl: NavController;
    time: string;
    appointment: AppointmentModel;
    customer: CustomerModel;
    client: ClientModel;
    tabBarElement: any;
    lastScheduledID :number;

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
        public customerService: CustomerServiceProvider, public toast: ToastServiceProvider,
        public clientService: ClientServiceProvider, public modalCtrl: ModalController,
        public appointmentService: AppointmentServiceProvider, private localNotifications: LocalNotifications) {
        this.rootNavCtrl = this.navParams.get('rootNavCtrl');
        this.appointment = this.appointmentService.getAppointment()
        this.client = this.clientService.getClient();
        this.customer = this.customerService.getCustomer();
        console.log("appointment is", this.appointment)
        this.time = this.navParams.get("appointments");
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        
       
    }

    ngOnInit(){
        this.localNotifications.getIds().then(ids =>{
            console.log("the list of ids",ids)
            if(ids.length > 0)   
            this.lastScheduledID = ids[ids.length -1 ]      
        }                 
        )
    }

    //To find is the appointment time is morning or evening
    isMorning(timeSlot) {
        let time = Array.from(timeSlot)
        // Rincy proper coding
        return (time[6] == "A")

    }

    submit(time) {
        this.appointment.aptSlot = time;
        this.appointmentService.setAppointment(this.appointment);

    }
    bookAppointment() {

        console.log("customer appointment", this.appointment.customerId)
        this.appointment.customerType = this.customer.customerType;
        this.appointment.sessionNumber = "0";
        // Rincy proper coding
        if (this.appointment.aptSlot) {
            // Post service to get the appointment
            // Sending the details as appointment  model
            this.appointmentService.bookAppointment(this.appointment).subscribe(res => {
                this.localNotifications.requestPermission().then((permission) => {
                let convertedTime = moment(this.appointment.aptSlot, 'hh:mm A').format('HH:mm')
                console.log(convertedTime);
                let sliceDate = this.appointment.aptDate.toLocaleDateString()
                let momentDate = moment(sliceDate + " " + convertedTime, 'DD/MM/YYYY HH:mm').subtract(10,'minutes').format()
                if(this.lastScheduledID ){
                    this.localNotifications.schedule([{
                        id:this.lastScheduledID +1,
                        text: 'You have an Appointment',
                        trigger: { at: new Date(momentDate) },
                        led: 'FF0000',
                        sound: null
                    }]);  
                } else{
                    this.localNotifications.schedule([{
                        id:1,
                        text: 'You have an Appointment',
                        trigger: { at: new Date(momentDate) },
                        led: 'FF0000',
                        sound: null
                    }]); 
                }        
                    
                });               

                this.rootNavCtrl.push(TokenPage, { "token": res }).then(() => {
                    this.tabBarElement.style.display = 'none';
                });
            }, error => this.onError(error))

        } else {
            this.toast.showAlert("Please select time !!!")
        }
    }

    //Handles error
    onError(error) {
        this.toast.dismissLoading();
        this.toast.showError(error);
    }

}