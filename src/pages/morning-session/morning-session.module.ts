import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MorningSessionPage } from './morning-session';

@NgModule({
  declarations: [
    MorningSessionPage,
  ],
  imports: [
    IonicPageModule.forChild(MorningSessionPage),
  ],
})
export class MorningSessionPageModule {}
