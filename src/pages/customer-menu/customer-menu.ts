import { ContactUsPage } from './../contact-us/contact-us';
import { ProfilePage } from './../profile/profile';
import { TabsPage } from './../tabs/tabs';
import { LoginPage } from './../login/login';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, ViewController, AlertController} from 'ionic-angular';
import { HelpPage } from '../help/help';
import { FaqPage } from '../faq/faq';
import { TermsAndConditionPage } from '../terms-and-condition/terms-and-condition';
import { AboutPage } from '../about/about';

export interface MenuItem {
  title: string;
  component: any;
  icon: string;

}

@IonicPage()
@Component({
  selector: 'page-customer-menu',
  templateUrl: 'customer-menu.html',
})
export class CustomerMenuPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = TabsPage;
  appMenuProvider: Array<MenuItem>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    private alertCtrl: AlertController) {

    this.appMenuProvider = [
      // {title: 'About Us', component: ContactUsPage, icon: 'mail'},
      { title: 'Home', component: TabsPage, icon: 'home' },
      // { title: 'Profile', component: ProfilePage, icon: 'document' },
      // { title: 'Contact Us', component: ContactUsPage, icon: 'mail' },
       { title: 'About Us', component: AboutPage, icon: 'information-circle' },
      { title: 'Help & Support', component: HelpPage, icon: 'help-circle' },
      { title: 'FAQ', component: FaqPage, icon: 'help-circle' },
      // { title: 'Terms & Conditions', component: TermsAndConditionPage, icon: 'document' }
    ];
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
 
  closeMenu() {

    let alert = this.alertCtrl.create({
      message: 'Do you want to log out?',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
            this.navCtrl.push(LoginPage).then(() => {
              const index = this.viewCtrl.index;
              this.navCtrl.remove(index);
            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

}
