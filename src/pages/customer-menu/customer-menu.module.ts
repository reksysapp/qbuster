import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomerMenuPage } from './customer-menu';

@NgModule({
  declarations: [
    CustomerMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomerMenuPage),
  ],
})
export class CustomerMenuPageModule {}
