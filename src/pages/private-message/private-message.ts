import { Storage } from '@ionic/storage';
import { ChatUserServiceProvider, UserInfo, ChatMessage } from './../../providers/chat-user-service/chat-user-service';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavParams, NavController } from 'ionic-angular';
import { Events, Content } from 'ionic-angular';
import { Socket } from 'ngx-socket-io';
import { DirectivesModule } from './../../directives/directives.module';
import { Observable } from 'rxjs/Observable';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { VideocallPage } from '../videocall/videocall';
import { AndroidPermissions } from '@ionic-native/android-permissions';


@IonicPage()
@Component({
  selector: 'page-private-message',
  templateUrl: 'private-message.html',
})
export class PrivateMessagePage {

  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: ElementRef;
  msgList: ChatMessage[] = [];
  user: UserInfo;
  toUser: UserInfo;
  editorMsg = '';
  showEmojiPicker = false;
  leavingChat: boolean;
  data: any;

  constructor(public storage: Storage, public event: Events, navParams: NavParams,
    private androidPermissions: AndroidPermissions,
    public navCtrl: NavController, private socket: Socket, public toast: ToastServiceProvider,
    private events: Events, public chatUserService: ChatUserServiceProvider) {
    console.log("data length:sun000");
    this.leavingChat = false;
    this.toUser = UserInfo.createBlank();

    console.log("TO USER IN CONS" + JSON.stringify(navParams.get("toUser")))

    this.data = navParams.get("toUser");
    this.chatUserService.getUserData(this.data.message.CUSTOMER_ID, this.data.message.CUSTOMER_NAME,this.data.message.CUSTOMER_DEVICE_ID)
      .then((res) => {
        this.user = res
        console.log("user customer PARAMS" + JSON.stringify(this.user))
      });

    this.chatUserService.getUserData(this.data.message.PROVIDER_ID, this.data.message.PROVIDER_NAME,this.data.message.PROVIDER_DEVICE_ID)
      .then((res) => {
        this.toUser = res
        console.log("To provider Params" + JSON.stringify(this.toUser))
      });

  }
  //To remove tab while opening the page
  //The page will display as model

  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    }
  }
  call() {
    this.navCtrl.push(VideocallPage, { "from": this.user, "to": this.toUser })
    // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
    //   result => {
    //     console.log('Has permission?', result.hasPermission)
    //     if (result.hasPermission == false) {
    //       this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.RECORD_AUDIO]).then(
    //         granted => {
    //           if(granted.hasPermission){
    //             // this.socket.emit('calling',this.toUser.deviceId)  
    //           this.navCtrl.push(VideocallPage, { "from": this.user, "to": this.toUser })
             
    //         }
    //           else
    //           this.navCtrl.pop();
    //         })
    //     }
    //     else{
    //       // this.socket.emit('calling',this.toUser.deviceId)  
    //       this.navCtrl.push(VideocallPage, { "from": this.user, "to": this.toUser })
     
    //     }
    // }
    // );
  }
  //To view tab while closing the page
  ionViewWillLeave() {
    //this.socket.disconnect();
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'flex';
      });
    }
    this.leavingChat = true;
    // this.socket.disconnect();
  }

  ngOnInit(): void {


    //has to be here
  }

  ionViewWillEnter() {
    console.log("data length:sun0");
    //data refreshed from server
    this.msgList = [];
    this.socket.on('output', (data) => {
      console.log("data length:" + data.length);
      if (data.length) {
        for (var x = 0; x < data.length; x++) {
          this.pushNewMsg(data[x]);
        }
      }
      this.editorMsg = '';
    });
    console.log("data length:sun1");

    //socket got disconnected
    this.socket.on('disconnect', (data) => {
      if (!this.leavingChat) this.toast.presentToast("Chat-No Internet Connection")
    });

    console.log("!!!!!!!!!!!!!!!!!!!! will enter start")
    this.socket.emit('load', { "user": this.user, "to": this.toUser })
    console.log("!!!!!!!!!!!!!!!!!!!! will enter end ")
    this.msgList = [];

    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    }
  }

  onFocus() {
    this.showEmojiPicker = false;
    this.content.resize();
    this.scrollToBottom();
  }

  switchEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
    if (!this.showEmojiPicker) {
      this.focus();
    } else {
      this.setTextareaScroll();
    }
    this.content.resize();
    this.scrollToBottom();
  }

  sendMsg() {
    if (!this.editorMsg.trim()) return;

    // Mock message
    const id = Date.now().toString();
    let newMsg: ChatMessage = {
      messageId: Date.now().toString(),
      userId: this.user._id, //lupin
      userName: this.user.name,
      userAvatar: this.user.avatar,
      toUserId: this.toUser._id, //clicked rani or anyone
      time: Date.now(),
      message: this.editorMsg,
      status: 'pending'
    };

    if (!this.showEmojiPicker) {
      this.focus();
    }
    newMsg.status = "success";
    console.log("chat user service ---------------- starts");
    // this.socket.emit('sendmsg', newMsg)  //storing data in database
    this.chatUserService.sendMsg(newMsg)
      .then(() => {
        let index = this.getMsgIndexById(id);
        if (index !== -1) {
          this.msgList[index].status = 'success';
        }
        // this.socket.emit('load', { "user": this.user, "to": this.toUser })
      }
      )
  }

  //loads chat in FE
  pushNewMsg(msg: ChatMessage) {
    console.log("INSIDE PUSH NEW MSG!!!!!")
    const userId = this.user._id,
      toUserId = this.toUser._id;

    // Verify user relationships
    if (msg.userId === userId && msg.toUserId === toUserId) {
      this.msgList.push(msg);
    } else if (msg.toUserId === userId && msg.userId === toUserId) {
      this.msgList.push(msg);
    }
    //this.scrollToBottom();  //NEED TO FIX THIS
  }

  getMsgIndexById(id: string) {
    return this.msgList.findIndex(e => e.messageId === id)
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.isScrolling) {
        this.content.scrollToBottom();
      }
    }, 400)
  }

  private focus() {
    if (this.messageInput && this.messageInput.nativeElement) {
      this.messageInput.nativeElement.focus();
    }
  }

  private setTextareaScroll() {
    const textarea = this.messageInput.nativeElement;
    textarea.scrollTop = textarea.scrollHeight;
  }
}
