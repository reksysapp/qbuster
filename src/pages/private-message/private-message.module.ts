import { ChatUserServiceProvider } from './../../providers/chat-user-service/chat-user-service';
import { RelativeTime } from './../../pipes/relative-time';
import { PrivateMessagePage } from './private-message';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmojiPickerComponentModule } from "../../components/emoji-picker/emoji-picker.module";
import { EmojiProvider } from "../../providers/emoji";

@NgModule({
  declarations: [
    PrivateMessagePage,
    RelativeTime
  ],
  imports: [
    EmojiPickerComponentModule,
    IonicPageModule.forChild(PrivateMessagePage),
  ],
  exports: [
    PrivateMessagePage
  ],
  providers: [
    ChatUserServiceProvider,
    EmojiProvider
  ]
})
export class PrivateMessagePageModule {}
