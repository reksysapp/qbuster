import { ChatPage } from './../chat/chat';
import { MyBookingPage } from './../my-booking/my-booking';
import { HomePage } from './../home/home';
import { Component, ViewChild } from '@angular/core';
import{ MenuController, NavParams, NavController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { Tabs } from 'ionic-angular/umd/navigation/nav-interfaces';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  @ViewChild('tabs') tabRef: Tabs;
  tab1Root = HomePage;
  tab2Root = MyBookingPage;
  tab3Root = ChatPage;
  tab4Root = ProfilePage;
  constructor(public  menu: MenuController , public navParams : NavParams, public navCtrl : NavController) {
    menu.enable(true);
    console.log("1      :")  
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }
  tabChanged($ev) {
    
    console.log("2      :",$ev) 
    $ev.setRoot($ev.root);
  }
}

