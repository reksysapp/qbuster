import { CustomerMemberPage } from './../customer-member/customer-member';
import { CustomerMenuPage } from './../customer-menu/customer-menu';
import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ActionSheetController } from 'ionic-angular';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { AppointmentModel } from './../../model/appointment-model';
import { CustomerModel } from '../../model/customer-model';
import { CustomerServiceProvider } from '../../providers/customer-service/customer-service';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { AlertController } from 'ionic-angular';
import { ClientModel } from './../../model/client-model';
import { JsonServicesProvider } from './../../providers/json-services/json-services';
import moment from 'moment';
import { ResetPasswordPage } from '../reset-password/reset-password';
import { environment } from './../../environment/environment';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { normalizeURL } from 'ionic-angular/util/util';
import { Crop } from '@ionic-native/crop';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  client: ClientModel;
  appointments: AppointmentModel[];
  customer: CustomerModel;
  stateList: any = [];
  citiesList: any = [];
  countryCode: string;
  date = moment().format();
  phoneString: string;
  imageData: any;
  imageUrl: any;
  private baseURL = environment.baseURL;
  nullCheck : boolean = false;
  //phone number regex future use
  // patternn =new RegExp("^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$");

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public toast: ToastServiceProvider, public alertCtrl: AlertController,
    public clientService: ClientServiceProvider, public customerService: CustomerServiceProvider,
    public jsonService: JsonServicesProvider, public viewCtrl: ViewController,private camera: Camera,
    public actionSheetCtrl: ActionSheetController,private crop: Crop, private cdRef: ChangeDetectorRef) {

    this.countryCode = '+91';
    this.customer = this.customerService.getCustomer();
    this.client = this.clientService.getClient();

    this.jsonService.getStatesList().subscribe(res => {
      this.stateList = res;
      // Rincy proper coding
      if (this.customer.state) {
        this.loadCities()
      }
    })
  }
  ionViewDidLoad() {
    this.imageUrl = this.baseURL + "customer/getImage/id/" + this.customer._id+ '?decache=' + Math.random();
  }
  ionViewWillEnter() {
    this.customer = this.customerService.getCustomer();
    console.log("customer inside view will enter   :", JSON.stringify(this.customer, null, 2))
  }
  save() {
    this.nullCheck = true;
    if(this.customer.phoneNum){
    this.phoneString = this.customer.phoneNum.toString();
    }
    else{}
    let email = /^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    // Rincy proper coding
    if (!email.test(String(this.customer.emailId).toLowerCase())) {
      this.toast.showAlert("Please enter a valid Email-id.")
    }
    // Rincy proper coding
    else if (this.customer.phoneNum && this.phoneString.length != 10) {
      this.toast.showAlert("Please enter a valid Mobile Number.")
    }
    // Rincy proper coding
    else if (this.customer.name && this.customer.phoneNum && this.customer.gender && this.customer.dob &&
      this.customer.state && this.customer.city && this.customer.name != "") {
      this.customer.emailId = this.customer.emailId.toLowerCase();

      this.customerService.updateCustomer(this.customer).subscribe(res => {
        this.navCtrl.push(CustomerMenuPage)
      }, error => this.onError(error))
    }
    else {
      this.toast.showAlert("Please fill mandatory fields.")
    }
  }

  //Load cities from json
  loadCities() {

    this.jsonService.getCitiesList().subscribe(res => {
      this.citiesList = res[this.customer.state.toString()]

      if (this.citiesList.indexOf(this.customer.city) > -1) {
        this.customer.city = this.customer.city
      }
      else {
        this.customer.city = null;
      }
    })

  }
  addMember() {
    this.nullCheck = true;
    this.phoneString = this.customer.phoneNum.toString();
    let email = /^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    // Rincy proper coding
    if (!email.test(String(this.customer.emailId).toLowerCase())) {
      this.toast.showAlert("Please enter a valid Email-id.")
    }

    else if (this.customer.phoneNum != null && this.phoneString.length != 10) {
      this.toast.showAlert("Please enter a valid Mobile Number.")
    }
    // Rincy proper coding
    else if (this.customer.name && this.customer.phoneNum && this.customer.gender && this.customer.dob &&
      this.customer.state && this.customer.city && this.customer.name != "") {
      this.customer.emailId = this.customer.emailId.toLowerCase();
      this.customerService.updateCustomer(this.customer).subscribe(res => {
        console.log("res inside save   :", JSON.stringify(res, null, 2))
        // this.customerService.getSecondaryCustomers(this.customer._id).subscribe(res => {
        //   console.log("res inside getSecondaryCustomers   :", JSON.stringify(res, null, 2))
        //   this.navCtrl.push(CustomerMemberPage, { "secondaryList": res })
        // }, error => this.onError(error))
        this.navCtrl.push(CustomerMemberPage)
      }, error => this.onError(error))
    }
    else {
      this.toast.showAlert("Please fill all the fields.")
    }
  }
  passwordChange() {
    console.log("password changed")
    this.navCtrl.push(ResetPasswordPage);
  }

  //Calls when user clicks on image
  //Is to add or update image
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Camera',
          role: 'destructive',
          handler: () => {
            this.takePhoto(1);
            console.log('Camera clicked');
          }
        },
        {
          text: 'Photo Library',
          handler: () => {
            this.takePhoto(0);
            console.log('Archive clicked');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }

  //To handle image which gets by choosing from gallery and from camera click
  takePhoto(sourceType) {
    const options: CameraOptions = {
      quality: 30,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: sourceType,
    }

    this.camera.getPicture(options).then((imageData) => {
      this.crop.crop(imageData, { quality: 30, targetWidth: 300 })
      .then(
        cropData => {
          console.log("crop data is ", cropData)
          this.customerService.uploadImage(normalizeURL(cropData), this.customer._id).then(res => {
            this.ionViewDidLoad();
            this.cdRef.detectChanges();
            console.log("========" + JSON.stringify(res))
          }).catch(err => {
            console.log("---------" + JSON.stringify(err))
          })
        },
        error => console.error('Error cropping image', error)
      );
    }, (err) => {
      // Handle error
      console.log("error   :", err)
    });
  }

  onError(error) {
    console.log('Inside OnError.')
    console.log(error);
    this.toast.showError(error);
  }
}
