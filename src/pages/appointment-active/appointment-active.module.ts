import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentActivePage } from './appointment-active';

@NgModule({
  declarations: [
    AppointmentActivePage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentActivePage),
  ],
})
export class AppointmentActivePageModule {}
