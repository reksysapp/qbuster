import { ViewTokenPage } from './../view-token/view-token';
import { AppointmentServiceProvider } from './../../providers/appointment-service/appointment-service';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ModalController, NavParams } from 'ionic-angular';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { AppointmentModel } from './../../model/appointment-model';
import { CustomerModel } from '../../model/customer-model';
import { CustomerServiceProvider } from '../../providers/customer-service/customer-service';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { AlertController } from 'ionic-angular';
import { ClientModel } from './../../model/client-model';
import { BookAppointmentPage } from '../book-appointment/book-appointment';

@Component({
  selector: 'page-appointment-active',
  templateUrl: 'appointment-active.html',
})
export class AppointmentActivePage {
  client: ClientModel;
  topTab: String;
  userData: any = [];
  date = new Date();
  appointments: AppointmentModel[];
  customer: CustomerModel;
  rootNavCtrl: NavController;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController,
    public toast: ToastServiceProvider, public alertCtrl: AlertController,
    public clientService: ClientServiceProvider, public customerService: CustomerServiceProvider,
    public appointmentService: AppointmentServiceProvider, public navParams: NavParams) {
    this.rootNavCtrl = this.navParams.get('rootNavCtrl');
    this.topTab = "active";


  }
  ionViewWillEnter() {
    // Gets appointment list of customer from DB
    this.customer = this.customerService.getCustomer();
    this.client = this.clientService.getClient();
    this.toast.showLoading("");
    //Gets total appointments and its details of the customer by sending customer id
    this.appointmentService.getTokenData(this.customer._id).subscribe(res => {
      this.toast.dismissLoading();
      this.appointments = res;
      console.log("the appointments are", JSON.stringify(this.appointments, null, 2))

    }, error => this.onError(error));
  }

  //To reshedule the appointment from active segment
  reshedule(item) {
    let alert = this.alertCtrl.create({
      title: 'Reshedule',
      message: 'Do you want to reshedule the appoinment?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            // Rincy null check
            if (((item || {}).provider || {}).providerType) {
              if (item.provider.providerType == "PRIMARY") {
                this.rootNavCtrl.push(BookAppointmentPage, {
                  "reshedule": {
                    "state": item.provider.providerDetails.state,
                    "city": item.provider.providerDetails.city, "clientName": item.provider.name,
                    "sector": item.provider.providerDetails.providerSector, "customerId": item.customerId, "providerId": item.providerId,
                    "client": item.provider, "phoneNum": item.provider.phoneNum,
                    "sessionType": item.provider.sessionDetails.sessionType, "sessionNum": item.sessionNumber,
                    "clientSession": item.provider.sessionDetails, "id": item._id, "date": item.aptDate,
                    "speciality": null, "specialist": null, "secondary": null, "customerType": item.customerType, "customerName": item.customer.name,
                    "notes": item.customerNotes
                  }
                })
              }
            }
            else {
              this.toast.showAlert("Application Problem");
            }
            if (((item || {}).provider || {}).providerType) {
              if (item.provider.providerType == "SECONDARY") {
                this.rootNavCtrl.push(BookAppointmentPage, {
                  "reshedule": {
                    "state": item.provider.providerDetails.state,
                    "city": item.provider.providerDetails.city, "clientName": item.provider.primaryProviderName,
                    "sector": item.provider.providerDetails.providerSector, "customerId": item.customerId, "providerId": item.providerId,
                    "client": item.provider, "phoneNum": item.provider.primaryProviderPhoneNum,
                    "sessionType": item.provider.sessionDetails.sessionType, "sessionNum": item.sessionNumber,
                    "clientSession": item.provider.sessionDetails, "id": item._id, "date": item.aptDate,
                    "speciality": item.provider.memberFields[1].refFieldValue, "specialist": item.provider.name, "secondary": [item.provider],
                    "customerType": item.customerType, "customerName": item.customer.name, "notes": item.customerNotes
                  }
                })
              }
            }
            else {
              this.toast.showAlert("Application Problem");
            }
          }

        }, {
          text: 'No',
          handler: () => {
          }
        }
      ]
    });
    alert.present();

  }
  //Compares the current date with each appointments date to find if it is active appointment or expaired appointment
  dateCompare(item) {
    // Rincy null check
    if (((item || {}).aptDate || {})) {
      var varDate = new Date(item.aptDate); //dd-mm-YYYY
      this.date.setHours(0, 0, 0, 0);


      // Rincy proper coding
      return (this.date <= varDate)
    }
    else {
      this.toast.showAlert("Application Problem");
    }
  }
  //To view the appointment details
  //By click it takes the purticular appointment to get in detailed view
  viewDetails(item) {
    console.log("click item", JSON.stringify(item))
    this.rootNavCtrl.push(ViewTokenPage, { "token": item })

  }
  //Handles error
  onError(error) {
    // console.log('Inside OnError.')
    this.toast.dismissLoading();
    console.log(error);
    this.toast.showError(error);
  }
}

