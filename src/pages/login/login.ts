import { ProviderLoginPage } from './../provider-login/provider-login';
import { CustomerLoginPage } from './../customer-login/customer-login';
import { Slides } from 'ionic-angular';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SuperTabs } from 'ionic2-super-tabs';
import { MenuController } from 'ionic-angular';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @ViewChild(Slides) slides: Slides;
  tab1Root = CustomerLoginPage;
  tab2Root = ProviderLoginPage;
  selectedTab = 0;
  slide: any[];

  @ViewChild(SuperTabs) superTabs: SuperTabs;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menu: MenuController) {
    this.slide = [
      {
        image: "assets/imgs/queue.jpg",
      },
      {
        image: "assets/imgs/image_in_hand2.jpg",
      }
    ]
  }
  ionViewDidEnter() {
  }

}