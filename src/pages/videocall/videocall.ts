import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WebrtcService } from '../../providers/WebrtcService';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Device } from '@ionic-native/device';
import { Socket } from 'ngx-socket-io';
import { ChatUserServiceProvider } from '../../providers/chat-user-service/chat-user-service';
@IonicPage()
@Component({
  selector: 'page-videocall',
  templateUrl: 'videocall.html',
})
export class VideocallPage {
  
  userName: any;

  topVideoFrame = 'partner-video';
  userId: string;
  partnerId: string;
  userDeviceId:string;
  partnerDeviceId:string;
  myEl: HTMLMediaElement;
  partnerEl: HTMLMediaElement;

  constructor(
    public webRTC: WebrtcService,
    public elRef: ElementRef,
    public navCtrl: NavController,
    public navParams: NavParams,
    private androidPermissions: AndroidPermissions,
    private socket: Socket,
    private chatservice:ChatUserServiceProvider
  ) { 
    
  }
 
  ngOnInit() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
      result => {
        console.log('Has permission?', result.hasPermission)
        if (result.hasPermission == false) {
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.RECORD_AUDIO]).then(
            granted => {
              if(granted.hasPermission){
               
                this.initialize();
             
            }
              else
              this.navCtrl.pop();
            })
        }
        else{
         
          this.initialize();
     
        }
    }
    );

   


    
  }
  initialize() {
    this.myEl = this.elRef.nativeElement.querySelector('#my-video');
    this.partnerEl = this.elRef.nativeElement.querySelector('#partner-video');
    this.userId = this.navParams.get("from")._id;
    this.userDeviceId=this.navParams.get("from").deviceId;
    this.partnerId = this.navParams.get("to")._id;
    this.partnerDeviceId=this.navParams.get("to").deviceId;
    this.userName=this.navParams.get("from").name;

    console.log("from",this.navParams.get("from"))
    console.log("to",this.navParams.get("to"))
    
    this.chatservice.updateProviderCustomer(this.userId,this.userName,this.partnerId,'yes').subscribe(res=>{
      console.log(res);
    })
 
    this.webRTC.init(this.userId, this.myEl, this.partnerEl).then(result=>{
      
      this.call();
    }).catch(Error)
      console.log(Error);
    
   
  }
  call() {

   console.log("calling!!!!!!!")
    this.webRTC.call(this.partnerId);
    this.swapVideo('my-video');
   
  
    
  }
  close() {
    this.webRTC.stopBothVideoAndAudio();
    this.navCtrl.pop();


  }
  swapVideo(topVideo: string) {
    this.topVideoFrame = topVideo;
  }
}
