
import { IonicPage, NavController, NavParams, PopoverController, AlertController } from 'ionic-angular';
import { ContactPage } from '../contact/contact';

import { Component, ElementRef, ViewChild } from '@angular/core';
import { CustomerMemberPage } from '../customer-member/customer-member';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@IonicPage()
@Component({
  selector: 'page-member-details',
  templateUrl: 'member-details.html',
})
export class MemberDetailsPage {

  memberForm: FormGroup;
  memberForm2: FormGroup;
  memberForm3: FormGroup;

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  currentMember: any = {}
  relationsList: any = [];
  phoneString: string;

  disable = "true";
  hideMe: boolean = true;
  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams, public popoverCtrl: PopoverController, public alertCtrl: AlertController, public toast: ToastServiceProvider, private formBuilder: FormBuilder) {
    console.log("the member is", this.navParams.get("members"))
    console.log("page loaded")
    this.currentMember = this.navParams.get("members")
    console.log("the member is", JSON.stringify(this.currentMember.name, null, 2));
    this.relationsList = ["Father", "Mother", "Son", "Daughter", "Spouse", "Others", "Self"]

    this.memberForm = this.formBuilder.group({

      //  phone: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      relation: ['', Validators.compose([Validators.required])],
      date: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.minLength(4), Validators.maxLength(10)])]

    });
    this.memberForm2 = this.formBuilder.group({

      phone: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      relation: ['', Validators.compose([Validators.required])],
      date: ['', Validators.compose([Validators.required])],
      // email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.minLength(4), Validators.maxLength(10)])]

    });
    this.memberForm3 = this.formBuilder.group({

      // phone: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      relation: ['', Validators.compose([Validators.required])],
      date: ['', Validators.compose([Validators.required])],
      // email: ['', Validators.compose([Validators.required])],
      // password: ['', Validators.compose([Validators.minLength(4), Validators.maxLength(10)])]

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MemberDetailsPage');
  }
  ionViewWillLeave() {
    //  this.currentMember =this.navParams.get("members")//
    /// this.navCtrl.push(CustomerMemberPage);
    console.log("the members is ", this.navParams.get("members"))
    this.currentMember = this.navParams.get("members")
    console.log("members", this.currentMember);

  }
  ionViewWillUnload() {
    this.currentMember = this.navParams.get("members")
  }

  hideShowPassword() {
    console.log("clicked")
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  toggleEdit() {
    this.disable = "false";
    this.hideMe = false;
  }
  onKeyUp(event: any) {
    let newValue = event.target.value;

    let regExp = new RegExp('^[1-9][0-9]*$');


    if (!regExp.test(newValue)) {
      event.target.value = newValue.slice(0, -1);
    }
  }
  

  save() {
    console.log("save clicked")

    // Rincy proper coding
    if (this.currentMember.emailId != null) {
      this.currentMember.emailId = this.currentMember.emailId.toLowerCase();
    }
    console.log("all")

    // this.customerService.updateCustomer(this.customer).subscribe(res => {
    //   this.navCtrl.push(CustomerMenuPage)
    // }, error => this.onError(error))


  }

  close() {
    // this.viewCtrl.dismiss(null);
    this.navCtrl.push(CustomerMemberPage).then(() => {
      this.viewCtrl.dismiss();
    });
  }
  back() {
    this.navCtrl.push(CustomerMemberPage).then(() => {
      this.viewCtrl.dismiss();
    });
  }

  /* delete the appointment */
  delete() {
    let alert = this.alertCtrl.create({
      // title: 'Confirm purchase',
      message: 'Do you want to Delete this account?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.navCtrl.push(CustomerMemberPage)
            // this.toast.showLoading("");
            // this.appointmentService.deleteAppoinment(this.token._id).subscribe(res => {
            //   this.navCtrl.parent.select(0);
            // }, error => this.onError(error))
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    alert.present();
  }
}
