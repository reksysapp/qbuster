import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServeTokenPage } from './serve-token';

@NgModule({
  declarations: [
    ServeTokenPage,
  ],
  imports: [
    IonicPageModule.forChild(ServeTokenPage),
  ],
})
export class ServeTokenPageModule {}
