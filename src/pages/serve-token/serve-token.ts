import { ProviderMenuPage } from './../provider-menu/provider-menu';
import { ServiceConformationPage } from './../service-conformation/service-conformation';
import { TokenSessionProvider } from './../../providers/token-session/token-session';
import { TokenSessionModel } from './../../model/token-session';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, ViewController } from 'ionic-angular';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { ClientModel } from './../../model/client-model';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';

@IonicPage()
@Component({
  selector: 'page-serve-token',
  templateUrl: 'serve-token.html',
})
export class ServeTokenPage {

  client: ClientModel;
  tokenSession: TokenSessionModel;
  myDate: string;
  aptInfo: any = [];
  session : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public clientService: ClientServiceProvider,
    public tokenSessionService: TokenSessionProvider, public toast: ToastServiceProvider, private alertCtrl: AlertController
    , public modalCtrl: ModalController, public viewCtrl: ViewController) {

    this.myDate = new Date().toISOString();
    this.client = this.clientService.getClient();
    this.tokenSession = this.tokenSessionService.getTokenSession();
    this.session = +this.tokenSession.currentDtls.session ;
    this.session = this.session + 1 ;

  }

  ionViewDidLoad() {
  }

  //To decrement token
  decrementToken() {
    this.toast.showLoading("");

    //FIXME -- do we need null check here
    this.tokenSessionService.tokenDecrement(this.tokenSession.currentDtls._id).subscribe(res => {
      this.toast.dismissLoading();
      this.tokenSession = res;
    }, error => this.onError(error))
  }
  //To increment token
  incrementToken() {
    
    this.toast.showLoading("");
    this.tokenSessionService.tokenIncrement(this.tokenSession.currentDtls._id).subscribe(res => {
      this.toast.dismissLoading();
      this.tokenSession = res;
    }, error => this.onError(error))
  }
  //To end session
  endSession() {
    let alert = this.alertCtrl.create({
      message: 'Do you want to end the session?',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.tokenSessionService.endSession(this.tokenSession.currentDtls._id).subscribe(res => {
              this.navCtrl.push(ProviderMenuPage)

            }, error => this.onError(error))
          }
        },   {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();

  }
  aptDtls(){
    // Rincy proper coding
    return (this.tokenSession.aptDtls)
  }
  completed() {
    let profileModal = this.modalCtrl.create(ServiceConformationPage);
    profileModal.onDidDismiss(data => {
      // Rincy proper coding
      if (data) {        
            this.tokenSession.aptDtls.providerStatus = data.providerStatus;       
      }
    });
    profileModal.present()
  }
  
  onError(error) {
    this.toast.dismissLoading();
    console.log('Inside OnError.')
    console.log(error);
    this.toast.showError(error);
  }
}
