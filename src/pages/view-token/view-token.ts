import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, App } from 'ionic-angular';
import { CustomerServiceProvider } from './../../providers/customer-service/customer-service';
import { ClientModel } from './../../model/client-model';
import { AppointmentServiceProvider } from './../../providers/appointment-service/appointment-service';
import { ClientServiceProvider } from './../../providers/client/client-service';
import { CustomerModel } from '../../model/customer-model';
import { TokenSessionProvider } from './../../providers/token-session/token-session';
import { ToastServiceProvider } from '../../providers/toast-service/toast-service';
import { BookAppointmentPage } from '../book-appointment/book-appointment';
import { PrivateMessagePage } from '../private-message/private-message';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { Storage } from '@ionic/storage';
import { _ParseAST } from '@angular/compiler';


@IonicPage()
@Component({
  selector: 'page-view-token',
  templateUrl: 'view-token.html',
})
export class ViewTokenPage {
  customer: CustomerModel;
  client: ClientModel;
  token: any = [];
  currentToken: any = [];
  root = this;
  reload: number;
  destination: any;
  currentLocation: any;
  long: any; //longitude
  lati: any; //latitude
  user: any
  start: string;
  constructor(public storage: Storage, public navCtrl: NavController, public navParams: NavParams, public clientService: ClientServiceProvider, public appointmentService: AppointmentServiceProvider,
    public customerService: CustomerServiceProvider, public viewCtrl: ViewController, public tokenSessionService: TokenSessionProvider,
    public toast: ToastServiceProvider, public alertCtrl: AlertController, public appCtrl: App, public geolocation: Geolocation, public plt: Platform, private iab: InAppBrowser,
    private launchNavigator: LaunchNavigator) {
      console.log("token" + JSON.stringify(navParams.get("token")))
    this.token = navParams.get("token");
    this.client = this.clientService.getClient();
    this.customer = this.customerService.getCustomer();

    this.currentToken = null;
    if (this.token.provider.sessionDetails.sessionType != 'appointment') {
      this.sessionCurrentToken();
    }

    //To initialize the ToUser for the chat(get his details from the provider table)
    this.clientService.getClientDetailsById(this.token.providerId).subscribe(res => {
      this.storage.set('toUser', res)
    })

    this.destination = this.token.provider.providerDetails.location;
    console.log("this.destination", this.destination)
    this.plt.ready().then(() => {

      //set options..
      var options = {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0
      };
      //use the geolocation to get the current position of the user
      this.geolocation.getCurrentPosition(options).then(data => {
        this.long = data.coords.longitude;
        this.lati = data.coords.latitude;
        this.start = this.lati + ',' + this.long;
      }).catch((err) => {
        console.log("Error", err);
      });
    });
  }

  chat() {
    console.log("this.token",this.token)
    const requestPayload ={message:{
      "CUSTOMER_ID":this.token.customerId,
      "PROVIDER_ID":this.token.providerId,
      "PROVIDER_NAME":this.token.provider.name,
      "CUSTOMER_NAME":this.token.customer.name,
      "CUSTOMER_DEVICE_ID":this.token.customer.deviceId,
      "PROVIDER_DEVICE_ID":this.token.provider.deviceId,
   }}
    this.navCtrl.push(PrivateMessagePage,{"toUser": requestPayload })
  }

  navigation() {
    this.getLocation();
  }

  ionViewWillEnter() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    }
  }
  //loads the navigator with both start and destination locations
  getLocation() {
    /* varun null check */
    //FIXME -- test it
    let locationExist =(((this.token || {}).provider || {}).providerDetails || {}).location;
    console.log("deep null check",locationExist)
    if (!locationExist) {
      this.toast.showAlert("Not available for this provider");
    }
    else {
      let options: LaunchNavigatorOptions = {
        start: this.start
      };

      this.launchNavigator.navigate(this.destination, options)
        .then(
          success => alert('Launched navigator'),
          error => alert('unable to launch navigator: ')
        );
    }
  }

  public sessionCurrentToken() {
    let CurrentTokenReq = { "providerId": this.token.providerId, "session": this.token.sessionNumber, "date": this.token.aptDate }
    this.tokenSessionService.sessionCurrentToken(CurrentTokenReq).subscribe(res => {
      this.currentToken = res

    }, error => console.log(error));

    this.reload = setTimeout(function () { this.sessionCurrentToken(); }.bind(this), 5000);
  }
  //To remove tab while opening the page
  //The page will display as model

  ngAfterViewInit() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    }
  }

  //To view tab while closing the page
  ionViewWillLeave() {
    clearTimeout(this.reload);
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'flex';
      });

    }
  }

  dismiss() {
    this.viewCtrl.dismiss().then(() => {
      clearTimeout(this.reload);
    })
  }

  // FIXME- varun

  current() {
    return (this.currentToken )
 }

  delete() {
    let alert = this.alertCtrl.create({
      message: 'Do you want to cancel the appointment?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.appointmentService.deleteAppoinment(this.token._id).subscribe(res => {
              this.navCtrl.parent.select(0);

            }, error => this.onError(error))
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();

  }

  reshedule() {
    let alert = this.alertCtrl.create({
      message: 'Do you want to reshedule the appointment?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            console.log("item inside reshedule    :", JSON.stringify(this.token, null, 2))

            //FIX ME -- we write switch insted of if here
            //Passing the resheduled appointment details as params
            /* varun null check */
            if (((this.token || {}).provider || {}).providerType ) {
              if (this.token.provider.providerType == "PRIMARY") {
                this.navCtrl.push(BookAppointmentPage, {
                  "reshedule": {
                    "state": this.token.provider.providerDetails.state,
                    "city": this.token.provider.providerDetails.city, "clientName": this.token.provider.name,
                    "sector": this.token.provider.providerDetails.providerSector, "customerId": this.token.customerId, "providerId": this.token.providerId,
                    "client": this.token.provider, "phoneNum": this.token.provider.phoneNum,
                    "sessionType": this.token.provider.sessionDetails.sessionType, "sessionNum": this.token.sessionNumber,
                    "clientSession": this.token.provider.sessionDetails, "id": this.token._id, "date": this.token.aptDate,
                    "speciality": null, "specialist": null, "secondary": null, "customerType": this.token.customerType, "customerName": this.token.customer.name
                    , "notes": this.token.customerNotes
                  }
                }).then(() => {
                  clearTimeout(this.reload);
                })
              }
            }
            else {
              this.toast.showAlert("Application Problem");
            }
            if (((this.token || {}).provider || {}).providerType ) {
              if (this.token.provider.providerType == "SECONDARY") {
                this.navCtrl.push(BookAppointmentPage, {
                  "reshedule": {
                    "state": this.token.provider.providerDetails.state,
                    "city": this.token.provider.providerDetails.city, "clientName": this.token.provider.primaryProviderName,
                    "sector": this.token.provider.providerDetails.providerSector, "customerId": this.token.customerId, "providerId": this.token.providerId,
                    "client": this.token.provider, "phoneNum": this.token.provider.phoneNum,
                    "sessionType": this.token.provider.sessionDetails.sessionType, "sessionNum": this.token.sessionNumber,
                    "clientSession": this.token.provider.sessionDetails, "id": this.token._id, "date": this.token.aptDate,
                    "speciality": this.token.provider.memberFields[1].refFieldValue, "specialist": this.token.provider.name, "secondary": [this.token.provider],
                    "customerType": this.token.customerType, "customerName": this.token.customer.name, "notes": this.token.customerNotes
                  }
                }).then(() => {
                  clearTimeout(this.reload);
                })
              }
            }
            else{
              this.toast.showAlert("Application Problem");
            }
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  // call() {
  //   let alert = this.alertCtrl.create({
  //     message: 'Do you want to connect with provider?',
  //     buttons: [
  //       {
  //         text: 'Yes',
  //         handler: () => {
  //           window.open("tel:" + this.token.provider.phoneNum);
  //           clearTimeout(this.reload);
  //         }
  //       }, {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         handler: () => {
  //         }
  //       }
  //     ]
  //   });
  //   alert.present();
  // }

  onError(error) {
    console.log(error);
    this.toast.showError(error);
  }
}
