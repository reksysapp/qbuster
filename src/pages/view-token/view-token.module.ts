import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewTokenPage } from './view-token';

@NgModule({
  declarations: [
    ViewTokenPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewTokenPage),
  ],
})
export class ViewTokenPageModule {}
